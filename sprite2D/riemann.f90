subroutine rpn2(ixy,maxm,meqn,mwaves,maux,mbc,mx,ql,qr,auxl,auxr,wave,s,amdq,apdq)
!
! solve Riemann problems for the system of equations q_t + (uq)_x = 0.

! waves: 1
! equations: number of chemical species

! Conserved quantities:
!       Number density of the chemical species

! On input, ql contains the state vector at the left edge of each cell
!           qr contains the state vector at the right edge of each cell
!
! This data is along a slice in the x-direction if ixy=1
!                            or the y-direction if ixy=2.
! On output, wave contains the waves, s the speeds,
! and amdq, apdq the decomposition of the flux difference
!     f(qr(i-1)) - f(ql(i))

! Note that the i'th Riemann problem has left state qr(:,i-1)
!                                   and right state ql(:,i)
    
    implicit none

    ! Input
    integer, intent(in) :: ixy, maxm, meqn, mwaves, mbc, mx, maux
    real(kind=8), intent(in) :: ql(meqn, 1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: qr(meqn, 1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: auxl(maux, 1-mbc:maxm+mbc)
    real(kind=8), intent(in) :: auxr(maux, 1-mbc:maxm+mbc)

    ! Output
    real(kind=8), intent(in out) :: wave(meqn, mwaves, 1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: s(mwaves, 1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: apdq(meqn, 1-mbc:maxm+mbc)
    real(kind=8), intent(in out) :: amdq(meqn, 1-mbc:maxm+mbc)

    ! Local storage
    integer :: i, mw

    real(kind=8) :: vdrift


    ! Initialize waves and speeds
    wave = 0.d0
    s = 0.d0
    amdq = 0.d0
    apdq = 0.d0

    ! Primary loop over grid cell interfaces
    do i = 2-mbc, mx+mbc
       ! Drift velocity field at the border of every cel
       vdrift = auxr(3 + ixy,i)
       s(1,i) = vdrift
       wave(1,1,i) = ql(1,i) - qr(1,i-1)
       amdq(1,i) = dmax1(s(1,i), 0.d0) * qr(1,i-1) + &
            dmin1(s(1,i), 0.d0) * ql(1,i)
       apdq(1,i) = -amdq(1,i)

    end do


end subroutine rpn2
