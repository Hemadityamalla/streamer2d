import os
import sys
import configparser
from scipy import interpolate
import numpy as np

config = configparser.ConfigParser()
config.sections()

class settings():
    def __init__(self,mobility_data='',mu_e=None,D_e=None):
        self.mobility_data = mobility_data
        self.mu_e= mu_e
        self.D_e = D_e

    def __call__(self):
        self.args, self.kwargs = _info_from_argv(sys.argv)
        if any('.cfg' in s for s in self.args):
            config_file = [s for s in self.args if ".cfg" in s]
            config.read(config_file)
        self.mx = int(config['Space_parameters']['mx'])
        self.my = int(config['Space_parameters']['my'])
        self.az = float(config['Space_parameters']['az'])
        self.bz = float(config['Space_parameters']['bz'])
        self.ar = float(config['Space_parameters']['ar'])
        self.br = float(config['Space_parameters']['br'])
        self.constant_density = config['Physical_parameters'].getboolean('constant_density')
        self.mean_pressure = float(config['Physical_parameters']['mean_pressure'])

    def interp_function(self,data_file):
        xdata, ydata = np.loadtxt(data_file,unpack=True)
        return interpolate.interp1d(xdata,ydata)


    def get_args(self,state=None,air=None):

        args, kwargs = self.args, self.kwargs

        if 'background_field' in kwargs:
            state.problem_data['Ebk'] = kwargs.get('background_field',0.0)
        else:
            state.problem_data['Ebk'] = float(config['Physical_parameters']['background_field'])

        if 'photoionization' in kwargs:
            state.problem_data['photo'] = kwargs.get('photoionization',False)
        else:
            state.problem_data['photo'] = config['Physical_parameters'].getboolean('photoionization')

        # Input data
        self.mobility_data = str(config['Input_data']['mobility_data'])
        self.diffusion_data = str(config['Input_data']['diffusion_data'])


    def poisson_solver(self):
        args, kwargs = self.args, self.kwargs
        self.free_zmax = config['Poisson_solver'].getboolean('free_zmax')
        self.free_zmin = config['Poisson_solver'].getboolean('free_zmin')
        if 'voltage' in kwargs:
            self.voltage = kwargs.get('voltage',0.0)
        else:
            self.voltage = float(config['Physical_parameters']['voltage'])

    def chem(self):
        args, kwargs = self.args, self.kwargs
        if 'vibrational' in kwargs:
            self.vib = kwargs.get('vibrational',False)
        else:
            self.vib =  config['Physical_parameters']['vibrational']
        if 'water_content' in kwargs:
            self.water_content = kwargs.get('water_content',0.0)
        else:
            self.water_content = float(config['Physical_parameters']['water_content'])
        if 'chem_folder' in kwargs:
            self.folder = kwargs.get('chem_folder')
        else:
            self.folder = str(config['Input_data']['chemistry_data'])


def _info_from_argv(argv=None):
    """Command-line -> method call arg processing.
    - positional args:
            a b -> method('a', 'b')
    - intifying args:
            a 123 -> method('a', 123)
    - json loading args:
            a '["pi", 3.14, null]' -> method('a', ['pi', 3.14, None])
    - keyword args:
            a foo=bar -> method('a', foo='bar')
    - using more of the above
            1234 'extras=["r2"]'  -> method(1234, extras=["r2"])
    @param argv {list} Command line arg list. Defaults to `sys.argv`.
    @returns (<method-name>, <args>, <kwargs>)
    """
    import json
    if argv is None:
        argv = sys.argv

    arg_strs = argv[1:]
    args = []
    kwargs = {}
    for s in arg_strs:
        if s.count('=') == 1:
            key, value = s.split('=', 1)
        else:
            key, value = None, s
        try:
            value = json.loads(value)
        except ValueError:
            pass
        if value=='True': value=True
        if value=='False': value=False
        if key:
            kwargs[key] = value
        else:
            args.append(value)

    return args, kwargs



if __name__=="__main__":
    get_args()
