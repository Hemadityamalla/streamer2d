from numpy import *

import numpy as np
import scipy.constants as co

import chemise as ch

MUN_E = 1e24
NAIR = 2.5e25

class HumidAir(ch.ReactionSet):



    DEFAULT_REDUCED_MOBILITIES = {

        'e'   : MUN_E}



    def __init__(self, water_content=0.0, folder ="chemise/swarm/streamer/streamer_dry/",vib=False, extend=False):

        super(HumidAir, self).__init__()



        self.water_content = water_content
        self.folder = folder
        self.vib = vib


        # Direct ionization and dissociative ionization
        self.add("e + N2 -> 2 * e + N2+ + V",

                 ch.LogLogInterpolate0(folder + "N2_Ionization0.dat", extend=False))


        self.add("e + O2 -> 2 * e + O2+",

                 ch.LogLogInterpolate0(folder + "O2_Ionization0.dat", extend=False))

        self.add("e + N2O -> 2 * e + N2O+",

                 ch.LogLogInterpolate0(folder + "N2O_Ionization0.dat", extend=False))

        self.add("e + NO -> 2 * e + NO+",

                 ch.LogLogInterpolate0(folder + "NO_Ionization0.dat", extend=False))


        # Direct (De-)excitation by electron impact, including those in which ionization can occur

        self.add("e + N2 -> e + N2v1",

                 ch.LogLogInterpolate0(folder + "N2_Excitation1.dat", extend=False))

        self.add("e + N2 -> e + N2v2",

                 ch.LogLogInterpolate0(folder + "N2_Excitation3.dat", extend=False))

        self.add("e + N2 -> e + N2v3",

                 ch.LogLogInterpolate0(folder + "N2_Excitation4.dat", extend=False))

        self.add("e + N2 -> e + N2v4",

                 ch.LogLogInterpolate0(folder + "N2_Excitation5.dat", extend=False))

        self.add("e + N2 -> e + N2v5",

                 ch.LogLogInterpolate0(folder + "N2_Excitation6.dat", extend=False))

        self.add("e + N2 -> e + N2v6",

                 ch.LogLogInterpolate0(folder + "N2_Excitation7.dat", extend=False))

        self.add("e + N2 -> e + N2v7",

                 ch.LogLogInterpolate0(folder + "N2_Excitation18.dat", extend=False))

        self.add("e + N2 -> e + N2v8",

                 ch.LogLogInterpolate0(folder + "N2_Excitation19.dat", extend=False))

        self.add("e + N2 -> e + N2v9",

                 ch.LogLogInterpolate0(folder + "N2_Excitation20.dat", extend=False))

        self.add("e + N2 -> e + N2v10",

                 ch.LogLogInterpolate0(folder + "N2_Excitation21.dat", extend=False))


        self.add("e + N2v1 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v1)_De-excitation0.dat", extend=False))

        self.add("e + N2v2 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v2)_De-excitation0.dat", extend=False))

        self.add("e + N2v3 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v3)_De-excitation0.dat", extend=False))

        self.add("e + N2v4 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v4)_De-excitation0.dat", extend=False))

        self.add("e + N2v5 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v5)_De-excitation0.dat", extend=False))

        self.add("e + N2v6 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v6)_De-excitation0.dat", extend=False))

        self.add("e + N2v7 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v7)_De-excitation0.dat", extend=False))

        self.add("e + N2v8 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v8)_De-excitation0.dat", extend=False))

        self.add("e + N2v9 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v9)_De-excitation0.dat", extend=False))

        self.add("e + N2v10 -> e + N2",

                 ch.LogLogInterpolate0(folder + "N2(v10)_De-excitation0.dat", extend=False))

        self.add("e + N2 -> 2 * e + N2+Bv0",

                 ch.LogLogInterpolate0(folder + "N2_Ionization1.dat", extend=False))

        self.add("e + N2 -> 2 * e + N2+Bv1",

                 ch.LogLogInterpolate0(folder + "N2_Ionization2.dat", extend=False))

        self.add("e + N2 -> 2 * e + N2+Bv2",

                 ch.LogLogInterpolate0(folder + "N2_Ionization3.dat", extend=False))

        self.add("e + N2 -> e + N2ap",

                 ch.LogLogInterpolate0(folder + "N2_Excitation50.dat", extend=False))
###
        self.add("e + N2 -> e + N + N",

                 ch.LogLogInterpolate0(folder + "N2_Excitation15.dat", extend=False))

        self.add("e + N2 -> e + N + ND",

                 ch.LogLogInterpolate0(folder + "N2_Excitation16.dat", extend=False))

        self.add("e + N2 -> e + N2app",

                 ch.LogLogInterpolate0(folder + "N2_Excitation14.dat", extend=False))

        self.add("e + N2 -> e + N2E",

                 ch.LogLogInterpolate0(folder + "N2_Excitation13.dat", extend=False))

        self.add("e + N2 -> e + N2w",

                 ch.LogLogInterpolate0(folder + "N2_Excitation52.dat", extend=False))

        self.add("e + N2 -> e + NP + N",

                 ch.LogLogInterpolate0(folder + "N2_Excitation17.dat", extend=False))

        self.add("e + N2 -> e + N2Ac",

                 ch.LogLogInterpolate0(folder + "N2_Excitation22.dat", extend=False))

        self.add("e + N2 -> e + N2Acv1",

                 ch.LogLogInterpolate0(folder + "N2_Excitation23.dat", extend=False))

        self.add("e + N2 -> e + N2Acv2",

                 ch.LogLogInterpolate0(folder + "N2_Excitation24.dat", extend=False))

        self.add("e + N2 -> e + N2Acv3",

                 ch.LogLogInterpolate0(folder + "N2_Excitation25.dat", extend=False))

        self.add("e + N2 -> e + N2Acv4",

                 ch.LogLogInterpolate0(folder + "N2_Excitation26.dat", extend=False))

        self.add("e + N2 -> e + N2Acv5",

                 ch.LogLogInterpolate0(folder + "N2_Excitation27.dat", extend=False))

        self.add("e + N2 -> e + N2Acv6",

                 ch.LogLogInterpolate0(folder + "N2_Excitation28.dat", extend=False))

        self.add("e + N2 -> e + N2Acv7",

                 ch.LogLogInterpolate0(folder + "N2_Excitation29.dat", extend=False))

        self.add("e + N2 -> e + N2Acv8",

                 ch.LogLogInterpolate0(folder + "N2_Excitation30.dat", extend=False))

        self.add("e + N2 -> e + N2Acv9",

                 ch.LogLogInterpolate0(folder + "N2_Excitation31.dat", extend=False))
        self.add("e + N2 -> e + N2Acv10",

                 ch.LogLogInterpolate0(folder + "N2_Excitation32.dat", extend=False))

        self.add("e + N2 -> e + N2Acv11",

                 ch.LogLogInterpolate0(folder + "N2_Excitation33.dat", extend=False))

        self.add("e + N2 -> e + N2Acv12",

                 ch.LogLogInterpolate0(folder + "N2_Excitation34.dat", extend=False))

        self.add("e + N2 -> e + N2Acv13",

                 ch.LogLogInterpolate0(folder + "N2_Excitation35.dat", extend=False))

        self.add("e + N2 -> e + N2Acv14",

                 ch.LogLogInterpolate0(folder + "N2_Excitation36.dat", extend=False))

        self.add("e + N2 -> e + N2Acv15",

                 ch.LogLogInterpolate0(folder + "N2_Excitation37.dat", extend=False))

        self.add("e + N2 -> e + N2Acv16",

                 ch.LogLogInterpolate0(folder + "N2_Excitation38.dat", extend=False))

        self.add("e + N2 -> e + N2B",

                 ch.LogLogInterpolate0(folder + "N2_Excitation40.dat", extend=False))

        self.add("e + N2 -> e + N2Bv1",

                 ch.LogLogInterpolate0(folder + "N2_Excitation41.dat", extend=False))

        self.add("e + N2 -> e + N2Bv2",

                 ch.LogLogInterpolate0(folder + "N2_Excitation42.dat", extend=False))

        self.add("e + N2 -> e + N2Bv3",

                 ch.LogLogInterpolate0(folder + "N2_Excitation43.dat", extend=False))

        self.add("e + N2 -> e + N2Bv4",

                 ch.LogLogInterpolate0(folder + "N2_Excitation44.dat", extend=False))

        self.add("e + N2 -> e + N2Bv5",

                 ch.LogLogInterpolate0(folder + "N2_Excitation45.dat", extend=False))

        self.add("e + N2 -> e + N2Bv6",

                 ch.LogLogInterpolate0(folder + "N2_Excitation46.dat", extend=False))

        self.add("e + N2 -> e + N2C",

                 ch.LogLogInterpolate0(folder + "N2_Excitation8.dat", extend=False))

        self.add("e + N2 -> e + N2Cv1",

                 ch.LogLogInterpolate0(folder + "N2_Excitation9.dat", extend=False))

        self.add("e + N2 -> e + N2Cv2",

                 ch.LogLogInterpolate0(folder + "N2_Excitation10.dat", extend=False))

        self.add("e + N2 -> e + N2Cv3",

                 ch.LogLogInterpolate0(folder + "N2_Excitation11.dat", extend=False))

        self.add("e + N2 -> e + N2Cv4",

                 ch.LogLogInterpolate0(folder + "N2_Excitation12.dat", extend=False))

        self.add("e + N2 -> e + N2a",

                 ch.LogLogInterpolate0(folder + "N2_Excitation51.dat", extend=False))

        self.add("e + N2 -> e + N2av1",

                 ch.LogLogInterpolate0(folder + "N2_Excitation53.dat", extend=False))

        self.add("e + N2 -> e + N2av2",

                 ch.LogLogInterpolate0(folder + "N2_Excitation54.dat", extend=False))

        self.add("e + N2 -> e + N2av3",

                 ch.LogLogInterpolate0(folder + "N2_Excitation55.dat", extend=False))

        self.add("e + N2 -> e + N2av4",

                 ch.LogLogInterpolate0(folder + "N2_Excitation56.dat", extend=False))

        self.add("e + N2 -> e + N2av5",

                 ch.LogLogInterpolate0(folder + "N2_Excitation57.dat", extend=False))

        self.add("e + N2 -> e + N2av6",

                 ch.LogLogInterpolate0(folder + "N2_Excitation58.dat", extend=False))

        self.add("e + N2 -> e + N2av7",

                 ch.LogLogInterpolate0(folder + "N2_Excitation59.dat", extend=False))

        self.add("e + N2 -> e + N2av8",

                 ch.LogLogInterpolate0(folder + "N2_Excitation60.dat", extend=False))

        self.add("e + N2 -> e + N2av9",

                 ch.LogLogInterpolate0(folder + "N2_Excitation61.dat", extend=False))

        self.add("e + N2 -> e + N2av10",

                 ch.LogLogInterpolate0(folder + "N2_Excitation62.dat", extend=False))

        self.add("e + N2 -> e + N2av11",

                 ch.LogLogInterpolate0(folder + "N2_Excitation63.dat", extend=False))

        self.add("e + N2 -> e + N2av12",

                 ch.LogLogInterpolate0(folder + "N2_Excitation64.dat", extend=False))

        self.add("e + N2 -> e + N2av13",

                 ch.LogLogInterpolate0(folder + "N2_Excitation65.dat", extend=False))

        self.add("e + N2 -> e + N2av14",

                 ch.LogLogInterpolate0(folder + "N2_Excitation66.dat", extend=False))

        self.add("e + N2 -> e + N2av15",

                 ch.LogLogInterpolate0(folder + "N2_Excitation67.dat", extend=False))

        self.add("e + N2 -> e + N2Wc",

                 ch.LogLogInterpolate0(folder + "N2_Excitation48.dat", extend=False))

        self.add("e + N2 -> e + N2Wcv1",

                 ch.LogLogInterpolate0(folder + "N2_Excitation68.dat", extend=False))

        self.add("e + N2 -> e + N2Wcv2",

                 ch.LogLogInterpolate0(folder + "N2_Excitation69.dat", extend=False))

        self.add("e + N2 -> e + N2Wcv3",

                 ch.LogLogInterpolate0(folder + "N2_Excitation70.dat", extend=False))

        self.add("e + N2 -> e + N2Bp",

                 ch.LogLogInterpolate0(folder + "N2_Excitation49.dat", extend=False))

        self.add("e + N2 -> e + N2Bpv1",

                 ch.LogLogInterpolate0(folder + "N2_Excitation71.dat", extend=False))

        self.add("e + O2 -> e + O2a",

                 ch.LogLogInterpolate0(folder + "O2_Excitation7.dat", extend=False))

        self.add("e + O2 -> e + O2b",

                 ch.LogLogInterpolate0(folder + "O2_Excitation8.dat", extend=False))


        self.add("e + O2 -> e + O + OS",

                 ch.LogLogInterpolate0(folder + "O2_Excitation15.dat", extend=False))

        self.add("e + O2 -> e + O + O3P",

                 ch.LogLogInterpolate0(folder + "O2_Excitation13.dat", extend=False))

        self.add("e + O2 -> e + O + O5P",

                 ch.LogLogInterpolate0(folder + "O2_Excitation14.dat", extend=False))


        self.add("e + O -> e + OD",

                 ch.LogLogInterpolate0(folder + "O_Excitation0.dat", extend=False))

        self.add("e + O -> e + OS",

                 ch.LogLogInterpolate0(folder + "O_Excitation1.dat", extend=False))

        self.add("e + O -> e + O3P",

                 ch.LogLogInterpolate0(folder + "O_Excitation2.dat", extend=False))

        self.add("e + O -> e + O5P",

                 ch.LogLogInterpolate0(folder + "O_Excitation3.dat", extend=False))



        # Dissociative attachment

        self.add("e + O2 -> O + O-",

                 ch.LogLogInterpolate0(folder + "O2_Attachment1.dat"), extend=False)

        self.add("e + O3 -> O2 + O-",

                 ch.LogLogInterpolate0(folder + "O3_Attachment1.dat"), extend=False)

        self.add("e + O3 -> O + O2-",

                 ch.LogLogInterpolate0(folder + "O3_Attachment0.dat"), extend=False)

        self.add("e + N2O -> N2 + O-",

                 ch.LogLogInterpolate0(folder + "N2O_Attachment0.dat"), extend=False)


        self.add("e + NO2 -> O- + NO",

                 ch.Constant(1e-11*co.centi**3),ref="Kossyi1992/PSST")


        # 3-body attachment
        self.add("e + O2 + O2 -> O2- + O2",

                 ch.LogLogInterpolate0(folder + "O2_Attachment0.dat",

                                       prefactor=(1 / co.centi**-3),

                                       extend=False))

        self.add("e + O2 + N2 -> O2- + N2",

                 ETemperatureExponential(1.07e-31 * co.centi**6, 70.0, 1500.0, 2.0),

                 ref="Kossyi1992/PSST")

        self.add("e + O + O2 -> O- + O2",

                 ch.Constant(1e-31*co.centi**6),ref="Kossyi1992/PSST")

        self.add("e + O + O2 -> O2- + O",

                 ch.Constant(1e-31*co.centi**6),ref="Kossyi1992/PSST")

        self.add("e + O3 + O2 -> O3- + O2",

                 ETemperatureExponential(1.4e-29 * co.centi**6, 600.0, 700.0, 1.0),

                 ref="Kossyi1992/PSST")


        self.add("e + NO + M -> NO- + M",

                 ch.Constant(1e-30*co.centi**6),ref="Kossyi1992/PSST")



        # Detachment
        self.add("M + O2- -> e + O2 + M",

                 PancheshnyiFitEN(1.24e-11 * co.centi**3, 179, 8.8),

                 ref="Pancheshnyi2013/JPhD")


        self.add("N2 + O- -> e + N2O",

                 PancheshnyiFitEN(1.16e-12 * co.centi**3, 48.9, 11),

                 ref="Pancheshnyi2013/JPhD")

        self.add("O2- + O2a -> e + O2 + O2",

                 ch.Constant(2.e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2- + O2b -> e + O2 + O2",

                 ch.Constant(3.6e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2- + N2Ac-> e + N2 + O2",

                 ch.Constant(2.1e-9*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2- + N2B -> e + N2 + O2",

                 ch.Constant(2.5e-9*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + O2a -> e + O3",

                 ch.Constant(3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + O2b -> e + O2 + O",

                 ch.Constant(6.9e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + N2Ac-> e + N2 + O",

                 ch.Constant(2.2e-9*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + N2B -> e + N2 + O",

                 ch.Constant(1.9e-9*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2- + O -> e + O3",

                 ch.Constant(1.5e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2- + N -> e + NO2",

                 ch.Constant(5e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + O -> e + O2",

                 ch.Constant(5e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + N -> e + NO",

                 ch.Constant(2.6e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O3- + O -> e + O2 + O2",

                 ch.Constant(3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NO2- + O -> e + NO3",

                 ch.Constant(1e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + O2 -> e + O3",

                 ch.Constant(5e-15*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + NO -> e + NO2",

                 ch.Constant(2.6e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + O3 -> e + O2 + O2",

                 ch.Constant(5e-10*co.centi**3),ref="Rodriguez1991")

        self.add("O- + CO -> e + CO2",

                 ch.Constant(5.27e-10*co.centi**3),ref="Biondi1971")

        self.add("O2- + O2b -> e + O2 + O2",

                 ch.Constant(3.6e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NO- + CO2 -> e + NO + CO2",

                 ch.Constant(8.3e-12*co.centi**3),ref="Albritton1978")

        self.add("NO- + CO -> e + NO + CO",

                 ch.Constant(5e-13*co.centi**3),ref="Albritton1978")

        self.add("NO- + N2O -> e + NO + N2O",

                 ch.Constant(5.1e-12*co.centi**3),ref="Albritton1978")

        self.add("NO- + NO -> e + NO + NO",

                 ch.Constant(5e-12*co.centi**3),ref="Albritton1978")


        # Electron-ion recombination

        self.add("e + N4+ -> N2 + N2",

                 ETemperaturePower(2e-6 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + O4+ -> O2 + O2",

                 ETemperaturePower(1.4e-6 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + N2+ -> N + N",

                 ETemperaturePower(2.8e-7 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + N2+ -> N + ND",

                 ETemperaturePower(2e-7 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + O2+ -> O + O",

                 ETemperaturePower(2e-7 * co.centi**3, 1.0),

                                   ref="Kossyi1992/PSST")

        self.add("e + NO+ -> N + O",

                 ETemperaturePower(4e-7 * co.centi**3, 1.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + NO+ -> O + ND",

                 ETemperaturePower(3e-7 * co.centi**3, 1.0),

                                   ref="Kossyi1992/PSST")


        self.add("e + N3+ -> N2 + N",

                 ETemperaturePower(2e-7 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + NO2+ -> NO + O",

                 ETemperaturePower(2e-7 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + N2O+ -> N2 + O",

                 ETemperaturePower(2e-7 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")


        self.add("e + N2O2+ -> N2 + O2",

                 ETemperaturePower(1.3e-6 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")


        self.add("e + N2O2+ -> N2 + O2",

                 ETemperaturePower(1.3e-6 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + N2NO+ -> NO + N2",

                 ETemperaturePower(1.3e-6 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")

        self.add("e + O2NO+ -> NO + O2",

                 ETemperaturePower(1.3e-6 * co.centi**3, 0.5),

                                   ref="Kossyi1992/PSST")


        # Negative ion conversion

        self.add("O2 + O- -> O2- + O",

                 PancheshnyiFitEN(6.96e-11 * co.centi**3.0, 198, 5.6),

                 ref="Pancheshnyi2013/JPhD")


        self.add("O2 + O- + M -> O3- + M",

                 PancheshnyiFitEN2(1.1e-30 * co.centi**6.0, 65),

                 ref="Pancheshnyi2013/JPhD")

        self.add("O2- + O -> O- + O2",

                 ch.Constant(3.3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + O2a -> O2- + O",

                 ch.Constant(1e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + O3 -> O3- + O",

                 ch.Constant(5.3e-11*co.centi**3),ref="BrasseurSolomon1986")


        self.add("O- + CO2 + M -> CO3- + M",

                 ch.Constant(2.53e-28*co.centi**6),ref="BrasseurSolomon1986")

        self.add("O2- + O2 + M -> O4- + M",

                 ch.Constant(3.5e-31*co.centi**6),ref="Kossyi1992/PSST")

        self.add("O2- + O3 -> O3- + O2",

                 ch.Constant(4e-10*co.centi**3),ref="BrasseurSolomon1986")

        self.add("O3- + O -> O2- + O2",

                 ch.Constant(3.2e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O3- + CO2 -> CO3- + O2",

                 ch.Constant(4.49e-10*co.centi**3),ref="BrasseurSolomon1986")

        self.add("O4- + O -> O3- + O2",

                 ch.Constant(4e-10*co.centi**3),ref="BrasseurSolomon1986")

        self.add("O4- + O2a -> O2- + O2+ + O2",

                 ch.Constant(3e-10*co.centi**3),ref="BrasseurSolomon1986")

        self.add("O4- + CO2 -> CO4- + O2",

                 ch.Constant(3.51e-10*co.centi**3),ref="BrasseurSolomon1986")

        self.add("CO3- + NO -> NO2- + CO2",

                 ch.Constant(8.98e-12*co.centi**3),ref="BrasseurSolomon1986")

        self.add("CO3- + NO2 -> NO3- + CO2",

                 ch.Constant(1.63e-10*co.centi**3),ref="BrasseurSolomon1986")

        self.add("CO3- + O -> O2- + CO2",

                 ch.Constant(8.98e-11*co.centi**3),ref="BrasseurSolomon1986")

        self.add("CO4- + O -> CO3- + O2",

                 ch.Constant(1.14e-10*co.centi**3),ref="BrasseurSolomon1986")

        self.add("CO4- + O3 -> O3- + CO2 + O2",

                 ch.Constant(1.06e-10*co.centi**3),ref="BrasseurSolomon1986")

        self.add("O- + N2O -> NO- + NO",

                 ch.Constant(2.1e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + NO2 -> NO2- + O",

                 ch.Constant(1.2e-9*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O- + CO2 + CO2 -> CO3- + CO2",

                 ch.Constant(2.53e-28*co.centi**6),ref="BrasseurSolomon1986")

        self.add("O2- + NO2 -> NO2- + O2",

                 ch.Constant(8e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O3- + NO -> NO3- + O",

                 ch.Constant(1e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O3- + NO2 -> NO2- + O3",

                 ch.Constant(7e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O3- + NO2 -> NO3- + O2",

                 ch.Constant(2e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O3- + NO -> NO2- + O2",

                 ch.Constant(2.6e-11*co.centi**3),ref="Capitelli2000")

        self.add("O3- + O3 -> O2 + O2 + O2 + e",

                 ch.Constant(1e-10*co.centi**3),ref="Kazil2003")

        self.add("NO- + O2 -> O2- + NO",

                 ch.Constant(5e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NO- + NO2 -> NO2- + O2",

                 ch.Constant(7.4e-16*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NO- + N2O -> NO2- + NO",

                 ch.Constant(2.8e-14*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NO2- + O3 -> NO3- + O2",

                 ch.Constant(1.8e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NO2- + NO2 -> NO3- + NO",

                 ch.Constant(4e-12*co.centi**3),ref="Capitelli2000")

        self.add("NO2- + NO3 -> NO3- + NO2",

                 ch.Constant(5e-10*co.centi**3),ref="Capitelli2000")

        self.add("NO3- + NO -> NO2- + NO2",

                 ch.Constant(3e-15*co.centi**3),ref="Capitelli2000")


        # Positive ion conversion
        self.add("N2+ + N2 + M -> N4+ + M",

                 TemperaturePower(5e-29 * co.centi**6, 2),

                 ref="Aleksandrov1999/PSST")

        self.add("N2+ + N + N2 -> N3+ + N2",

                 ch.Constant(3.41e-29*co.centi**6),ref="Kossyi1992/PSST")

        self.add("O2+ + O2 + M -> O4+ + M",

                 TemperaturePower(2.4e-30 * co.centi**6, 3),

                 ref="Aleksandrov1999/PSST")

        self.add("O2+ + N2 + N2 -> N2O2+ + N2",

                 ch.Constant(0.9e-30*co.centi**6),ref="Kossyi1992/PSST")

        self.add("NO+ + N2 + N2 -> N2NO+ + N2",

                 ch.Constant(2e-31*co.centi**6),ref="Kossyi1992/PSST")

        self.add("NO+ + O2 + N2 -> O2NO+ + N2",

                 ch.Constant(3e-31*co.centi**6),ref="Kossyi1992/PSST")

        self.add("NO+ + O2 + O2 -> O2NO+ + O2",

                 ch.Constant(0.9e-31*co.centi**6),ref="Kossyi1992/PSST")

        self.add("N2+ + O2 -> O2+ + N2",

                 ch.Constant(6e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2+ + N2 -> NO+ + NO",

                 ch.Constant(1e-17*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2+ + N -> NO+ + O",

                 ch.Constant(1.2e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O4+ + O2 -> O2+ + O2 + O2",

                 ch.Constant(1.72e-13*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O4+ + O2a -> O2+ + O2 + O2",

                 ch.Constant(1e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O4+ + O2b -> O2+ + O2 + O2",

                 ch.Constant(1e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O4+ + O -> O2+ + O3",

                 ch.Constant(3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O4+ + NO -> NO+ + O2 + O2",

                 ch.Constant(1e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N4+ + N2 -> N2+ + N2 + N2",

                ch.Constant(2.51e-15*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N4+ + O2 -> 2 * N2 + O2+",

                 TemperaturePower(2.5e-10 * co.centi**3, 3),

                 ref="Aleksandrov1999/PSST")

        self.add("N+ + O2 -> O2+ + N",

                 ch.Constant(2e-10*co.centi**3),ref="Zinn1990")

        self.add("N+ + O2 -> O2+ + ND",

                 ch.Constant(8.4e-11*co.centi**3),ref="Zinn1990")

        self.add("N+ + O2 -> NO+ + O",

                 ch.Constant(5e-11*co.centi**3),ref="Zinn1990")

        self.add("N+ + O2 -> NO+ + OD",

                 ch.Constant(2e-10*co.centi**3),ref="Zinn1990")

        self.add("N+ + O2 -> O+ + NO",

                 ch.Constant(2.8e-11*co.centi**3),ref="Zinn1990")

        self.add("O+ + O2 -> O2+ + O",

                 ch.Constant(2e-11*co.centi**3),ref="Zinn1990")

        self.add("O+ + N2 -> NO+ + N",

                 ch.Constant(1.8e-12*co.centi**3),ref="Zinn1990")

        self.add("O+ + N2 + O2 -> NO+ + N + O2",

                 ch.Constant(1.35e-28*co.centi**6),ref="HerronGreen2001")

        self.add("O+ + N2 + N2 -> NO+ + N + N2",

                 ch.Constant(1.35e-28*co.centi**6),ref="HerronGreen2001")


        self.add("N3+ + O2 -> O2+ + N + N2",

                 ch.Constant(2.3e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N3+ + O2 -> NO2+ + N2",

                 ch.Constant(4.4e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N3+ + NO -> N2O+ + N2",

                 ch.Constant(7e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N+ + N2 + N2 -> N3+ + N2",

                 ch.Constant(3.41e-29*co.centi**6),ref="Kossyi1992/PSST")

        self.add("N+ + O + M -> NO+ + M",

                 ch.Constant(1e-29*co.centi**6),ref="Kossyi1992/PSST")

        self.add("N+ + N + M -> N2+ + M",

                 ch.Constant(6e-29*co.centi**6),ref="Kossyi1992/PSST")

        self.add("N+ + O -> O+ + N",

                 ch.Constant(1e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N+ + O3 -> NO+ + O2",

                 ch.Constant(5e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N+ + NO -> NO+ + N",

                 ch.Constant(8e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N+ + NO -> N2+ + O",

                 ch.Constant(3e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N+ + NO -> O+ + N2",

                 ch.Constant(1e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N+ + N2O -> NO+ + N2",

                 ch.Constant(5.5e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2+ + O -> O+ + N2",

                 ch.Constant(1.08e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2+ + O -> NO+ + N",

                 ch.Constant(1.59e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2+ + O3 -> O2+ + O + N2",

                 ch.Constant(1e-10*co.centi**3),ref="Capitelli2000")

        self.add("N2+ + N2O -> N2O+ + N2",

                 ch.Constant(5e-10*co.centi**3),ref="Capitelli2000")

        self.add("N2+ + N2O -> NO+ + N + N2",

                 ch.Constant(4e-10*co.centi**3),ref="Capitelli2000")

        self.add("N2+ + NO -> NO+ + N2",

                 ch.Constant(3.3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N3+ + N2A -> N3+ + N2",

                 ch.Constant(3e-10*co.centi**3),ref="Starikovskaia2001")

        self.add("N3+ + N -> N2+ + N2",

                 ch.Constant(6.6e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N3+ + NO -> NO+ + N + N2",

                 ch.Constant(7e-11*co.centi**3),ref="Starikovskaia2001")

        self.add("N4+ + O -> O+ + N2 + N2",

                 ch.Constant(2.5e-10*co.centi**3),ref="Capitelli2000")

        self.add("O+ + O3 -> O2+ + O2",

                 ch.Constant(1e-10*co.centi**3),ref="Capitelli2000")

        self.add("O+ + NO -> NO+ + O",

                 ch.Constant(2.4e-11*co.centi**3),ref="Capitelli2000")

        self.add("O+ + NO -> O2+ + N",

                 ch.Constant(3e-12*co.centi**3),ref="Capitelli2000")

        self.add("O+ + N2O -> NO+ + NO",

                 ch.Constant(2.3e-10*co.centi**3),ref="Capitelli2000")

        self.add("O+ + N2O -> O+ + N2",

                 ch.Constant(2e-11*co.centi**3),ref="Capitelli2000")

        self.add("O+ + N2O -> N2O+ + O",

                 ch.Constant(2.2e-11*co.centi**3),ref="Capitelli2000")

        self.add("O+ + NO2 -> NO2+ + O",

                 ch.Constant(1.6e-9*co.centi**3),ref="Capitelli2000")

        self.add("O2+ + NO2 -> NO+ + O3",

                 ch.Constant(1e-11*co.centi**3),ref="Capitelli2000")

        self.add("O2+ + NO2 -> NO2+ + O2",

                 ch.Constant(6.6e-10*co.centi**3),ref="Capitelli2000")

        self.add("O4+ + N2 -> N2O2+ + O2",

                 ch.Constant(2.23e-17*co.centi**3),ref="Capitelli2000")

        self.add("NO2+ + NO -> NO+ + NO2",

                 ch.Constant(2.9e-10*co.centi**3),ref="Capitelli2000")

        self.add("N2O+ + NO -> NO+ + N2O",

                 ch.Constant(2.9e-10*co.centi**3),ref="Capitelli2000")


        self.add("N2O2+ + N2 -> O2+ + N2 + N2",

                 ch.Constant(7.19e-11*co.centi**3),ref="Capitelli2000")


        self.add("N2O2+ + O2 -> O4+ + N2",

                 ch.Constant(1e-10*co.centi**3),ref="Capitelli2000")

        self.add("e + N2NO+ -> NO + N2",

                 ch.Constant(1.59e-9*co.centi**3),ref="Kossyi1992/PSST")


        # Neutral kinetics

        self.add("N2Ac + O2 -> N2O + O3P",

                 ch.Constant(2e-14*co.centi**3),ref="Gordiets1995")

        self.add("N2Ac + O3P -> NO + ND",

                 ch.Constant(7e-12*co.centi**3),ref="Gordiets1995")

        self.add("N2Ac + N2O -> N2 + N + NO",

                 ch.Constant(1e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2Ac + O2 -> N2 + O2a",

                 ch.Constant(2e-13*co.centi**3),ref="Gordiets1995")

        self.add("N2Ac + O2 -> N2 + O2b",

                 ch.Constant(2e-13*co.centi**3),ref="Gordiets1995")

        self.add("N2Ac + O2a -> N2B + O2",

                 ch.Constant(1e-10*co.centi**3),ref="Kamaratos2006")

        self.add("N2Ac + N2Ac -> N2C + N2",

                 ch.Constant(1.5e-10*co.centi**3),ref="Gordiets1995")

        self.add("N2Ac + N2Ac -> N2B + N2",

                 ch.Constant(3e-10*co.centi**3),ref="Gordiets1995")

        self.add("N2Ac + N -> N2 + NP",

                 ch.Constant(5e-11*co.centi**3),ref="Gordiets1995")

        self.add("N2Ac + O3P -> N2 + OS",

                 ch.Constant(2e-11*co.centi**3),ref="Gordiets1995")

        self.add("N2Ac + NO -> N2 + NO",

                 ch.Constant(2e-11*co.centi**3),ref="Kossyi1992/PSST")


        self.add("N2B + NO -> N2Ac + NO",

                 ch.Constant(2.4e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2B + O2 -> N2Ac + O + O",

                 ch.Constant(3e-10*co.centi**3),ref="Kossyi1992/PSST")


        self.add("N2a + N2 -> N2B + N2",

                 ch.Constant(2e-13*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2a + O2 -> N2 + O + O",

                 ch.Constant(2.8e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2a + N2Ac -> e + N4+",

                 ch.Constant(1.5e-11*co.centi**3),ref="Gordiets1995")

        self.add("N2a + N2a -> e + N4+",

                 ch.Constant(1e-11*co.centi**3),ref="Gordiets1995")

        self.add("N2C + N2 -> N2a + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2C + O2 -> N2 + OS + O",

                 ch.Constant(3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2a + O3 -> O2 + O2 + O",

                 ch.Constant(3.9e-16*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2a + N -> NO + O",

                 ch.Constant(9.96e-17*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2a + N2 -> N2 + O2",

                 ch.Constant(3e-21*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2a + O2 -> O2 + O2",

                 ch.Constant(1.59e-18*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2a + O -> O2 + O",

                 ch.Constant(7e-16*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2a + NO -> O2 + NO",

                 ch.Constant(2.46e-17*co.centi**3),ref="Yaron1976")

        self.add("O2a + O3 -> O2 + O2 + OD",

                 ch.Constant(3.54e-17*co.centi**3),ref="Gordiets1995")

        self.add("O2a + O2a + O2-> O3 + O3",

                 ch.Constant(1e-31*co.centi**6),ref="Gordiets1995")

        self.add("O2b + O3 -> O2 + O2 + O",

                 ch.Constant(1.8e-11*co.centi**3),ref="Gordiets1995")

        self.add("O2b + N2 -> O2a + N2",

                 ch.Constant(1.38e-15*co.centi**3),ref="Kossyi1992/PSST")


        self.add("O2b + O2 -> O2a + O2",

                 ch.Constant(4.29e-17*co.centi**3),ref="Kossyi1992/PSST")


        self.add("O2b + O -> O2a + O",

                 ch.Constant(8e-14*co.centi**3),ref="Kossyi1992/PSST")


        self.add("O2b + NO -> O2a + NO",

                 ch.Constant(4e-14*co.centi**3),ref="Kossyi1992/PSST")


        self.add("ND + O2 -> NO + O",

                 ch.Constant(5.2e-12*co.centi**3),ref="Gordiets1995")

        self.add("ND + O2 -> NO + OD",

                 ch.Constant(4.9e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("ND + NO -> N2O",

                 ch.Constant(6e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("ND + N2O -> NO + N2",

                 ch.Constant(3.5e-12*co.centi**3),ref="Gordiets1995")

        self.add("ND + N2 -> N + N2",

                 ch.Constant(6e-15*co.centi**3),ref="Kossyi1992/PSST")

        self.add("ND + O -> N + OD",

                 ch.Constant(4e-13*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NP + O2 -> NO + O",

                 ch.Constant(2.6e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NP + NO -> N2Ac + O",

                 ch.Constant(3.4e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NP + N2 -> ND + N2",

                 ch.Constant(2e-18*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NP + N -> ND + N",

                 ch.Constant(1.8e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + N2 -> O + N2",

                 ch.Constant(3.07e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + O2 -> O + O2b",

                 ch.Constant(3.58e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + N2 -> O + O2",

                 ch.Constant(8.95e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + O3 -> O + O + O2",

                 ch.Constant(1.2e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + O3 -> O2 + O2",

                 ch.Constant(1.2e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + NO -> N + O2",

                 ch.Constant(1.2e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + N2O -> NO + NO",

                 ch.Constant(7.2e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + N2O -> O2 + N2",

                 ch.Constant(4.4e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + N2 -> O + N2",

                 ch.Constant(5e-17*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O2 -> O + O2Ac",

                 ch.Constant(6.13e-14*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O2 -> OD + O2",

                 ch.Constant(6.13e-14*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O3 -> OD + O + O2",

                 ch.Constant(5.8e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O3 -> O2 + O2",

                 ch.Constant(5.8e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + NO -> O + NO",

                 ch.Constant(1.8e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + NO -> OD + NO",

                 ch.Constant(3.2e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + N2O -> O + N2O",

                 ch.Constant(6.2e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + N2O -> OD + N2O",

                 ch.Constant(3.1e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O2a -> O + O + O",

                 ch.Constant(3.4e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O2a -> O + O2A",

                 ch.Constant(1.3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O -> OD + O",

                 ch.Constant(1.11e-11*co.centi**3),ref="Kossyi1992/PSST")

        '''self.add("OS + H2O -> OH + OH",

                 ch.Constant(1.8e-10*co.centi**3),ref="BrasseurSolomon1986")'''

        self.add("ND + O -> N + O",

                 ch.Constant(5.51e-13*co.centi**3),ref="BrasseurSolomon1986")

        '''self.add("OHex + O -> H + O2",

                 ch.Constant(3.06e-10*co.centi**3),ref="Makhlouf1995")

        self.add("OHex + M -> OH + M",

                 ch.Constant(1.22e-13*co.centi**3),ref="Sentman2008")'''

        self.add("ND + NO -> N2 + O",

                 ch.Constant(1.8e-10*co.centi**3),ref="Capitelli2000")

        self.add("NP + NO -> N2 + O",

                 ch.Constant(3e-11*co.centi**3),ref="Capitelli2000")

        self.add("N2a + NO -> N2 + N + O",

                 ch.Constant(3.6e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2a + O2 -> N2 + O + OD",

                 ch.Constant(2.8e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2C + N2 -> N2B + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2C + O2 -> N2 + O + OD",

                 ch.Constant(2.5e-10*co.centi**3),ref="Capitelli2000")

        self.add("N2Ac + O2 -> N2 + O + O",

                 ch.Constant(2.54e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2Ac + N -> N2 + N",

                 ch.Constant(2.2e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("N2Ac + NO -> N2 + NOA",

                 ch.Constant(8.75e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + O2 -> O + O2a",

                 ch.Constant(1e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + N2O -> N2O + O",

                 ch.Constant(1e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + NO2 -> NO + O2",

                 ch.Constant(3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + N2O -> N2O + O",

                 ch.Constant(1e-12*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OD + O3 -> O + O3",

                 ch.Constant(2.41e-10*co.centi**3),ref="LinstromMallard2015")

        self.add("OD + CO2 -> O + CO2",

                 ch.Constant(1.35e-10*co.centi**3),ref="LinstromMallard2015")

        self.add("OD + CO -> CO2",

                 ch.Constant(7.3e-11*co.centi**3),ref="LinstromMallard2015")

        self.add("OS + O2 -> O + O2",

                 ch.Constant(6.13e-14*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O2a -> O2 + OD",

                 ch.Constant(3.6e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O2a -> O + O2b",

                 ch.Constant(1.3e-10*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + O -> OD + OD",

                 ch.Constant(1.11e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("OS + CO2 -> O + CO2",

                 ch.Constant(3.09e-13*co.centi**3),ref="LinstromMallard2015")

        self.add("O2a + NO -> O + NO2",

                 ch.Constant(4.88e-18*co.centi**3),ref="LinstromMallard2015")

        self.add("O2b + O -> O2 + O",

                 ch.Constant(1.88e-14*co.centi**3),ref="Kossyi1992/PSST")

        self.add("O2b + O3 -> O2a + O2a + O",

                 ch.Constant(1.8e-11*co.centi**3),ref="Kossyi1992/PSST")

        self.add("NOA + O2 -> NO + O2",

                 ch.Constant(1.62e-10*co.centi**3),ref="Simek2013")

        self.add("NOA + N2 -> NO + N2Ac",

                 ch.Constant(5e-14*co.centi**3),ref="Thoman1992")

        # 6 new reactions

        self.add("N + O2 -> NO + O",

                 TemperatureExponential(4.5e-12 * co.centi**3, -3220.0),

                                   ref="Kossyi1992/PSST")

        self.add("N + NO2 -> O + O + N2",

                 ch.Constant(9.1e-13*co.centi**3),ref="Capitelli2000")

        self.add("N + NO2 -> O + N2O",

                 ch.Constant(3e-12*co.centi**3),ref="Capitelli2000")

        self.add("N + NO2 -> O2 + N2",

                 ch.Constant(7e-13*co.centi**3),ref="Capitelli2000")

        self.add("N + NO2 -> NO + NO",

                 ch.Constant(2.3e-13*co.centi**3),ref="Capitelli2000")

        self.add("O + NO2 -> O2 + NO",

                 ch.Constant(8.5e-12*co.centi**3),ref="Kossyi1992/PSST")


        # VT and VV

        self.add("N2v1 + N2 -> N2 + N2",

                 ch.Constant(3.5e-21*co.centi**3),ref="Kurnosov2007")

        self.add("N2 + N2 -> N2v1 + N2",

                 ch.Constant(1.63e-28*co.centi**3),ref="Kurnosov2007")

        self.add("N2v2 + N2 -> N2v1 + N2",

                 ch.Constant(6.5e-21*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2 -> N2v2 + N2",

                 ch.Constant(1.9e-28*co.centi**3),ref="Kurnosov2007")

        self.add("N2v3 + N2 -> N2v2 + N2",

                 ch.Constant(1.5e-20*co.centi**3),ref="Kurnosov2007")

        self.add("N2v2 + N2 -> N2v3 + N2",

                 ch.Constant(7.38e-28*co.centi**3),ref="Kurnosov2007")

        self.add("N2v4 + N2 -> N2v3 + N2",

                 ch.Constant(2.5e-20*co.centi**3),ref="Kurnosov2007")

        self.add("N2v3 + N2 -> N2v4 + N2",

                 ch.Constant(1.23e-27*co.centi**3),ref="Kurnosov2007")

        self.add("N2v5 + N2 -> N2v4 + N2",

                 ch.Constant(3.5e-20*co.centi**3),ref="Kurnosov2007")

        self.add("N2v4 + N2 -> N2v5 + N2",

                 ch.Constant(9.64e-28*co.centi**3),ref="Kurnosov2007")

        self.add("N2v6 + N2 -> N2v5 + N2",

                 ch.Constant(7e-20*co.centi**3),ref="Kurnosov2007")

        self.add("N2v5 + N2 -> N2v6 + N2",

                 ch.Constant(3.45e-27*co.centi**3),ref="Kurnosov2007")

        self.add("N2v7 + N2 -> N2v6 + N2",

                 ch.Constant(1e-19*co.centi**3),ref="Kurnosov2007")

        self.add("N2v6 + N2 -> N2v7 + N2",

                 ch.Constant(2.76e-27*co.centi**3),ref="Kurnosov2007")

        self.add("N2v8 + N2 -> N2v7 + N2",

                 ch.Constant(1e-19*co.centi**3),ref="Kurnosov2007")

        self.add("N2v7 + N2 -> N2v8 + N2",

                 ch.Constant(4.92e-27*co.centi**3),ref="Kurnosov2007")

        self.add("N2v9 + N2 -> N2v8 + N2",

                 ch.Constant(2e-19*co.centi**3),ref="Kurnosov2007")

        self.add("N2v8 + N2 -> N2v9 + N2",

                 ch.Constant(1e-25*co.centi**3),ref="Kurnosov2007")

        self.add("N2v10 + N2 -> N2v9 + N2",

                 ch.Constant(2e-19*co.centi**3),ref="Kurnosov2007")

        self.add("N2v9 + N2 -> N2v10 + N2",

                 ch.Constant(1.82e-24*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2v1 -> N2v2 + N2",

                 ch.Constant(1e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v2 + N2 -> N2v1 + N2v1",

                 ch.Constant(6.29e-15*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2v2 -> N2v3 + N2",

                 ch.Constant(4e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v3 + N2 -> N2v1 + N2v2",

                 ch.Constant(4.24e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2v3 -> N2v4 + N2",

                 ch.Constant(3e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v4 + N2 -> N2v1 + N2v3",

                 ch.Constant(5.3e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2v4 -> N2v5 + N2",

                 ch.Constant(5.6e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v5 + N2 -> N2v1 + N2v4",

                 ch.Constant(3.32e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2v5 -> N2v6 + N2",

                 ch.Constant(6e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v6 + N2 -> N2v1 + N2v5",

                 ch.Constant(6.36e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2v6 -> N2v7 + N2",

                 ch.Constant(5.6e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v7 + N2 -> N2v1 + N2v6",

                 ch.Constant(3.32e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2v7 -> N2v8 + N2",

                 ch.Constant(5e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v8 + N2 -> N2v1 + N2v7",

                 ch.Constant(5.3e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v1 + N2v8 -> N2v9 + N2",

                 ch.Constant(1.39e-14*co.centi**3),ref="Kurnosov2007")

        self.add("N2v9 + N2 -> N2v1 + N2v8",

                 ch.Constant(1.5e-13*co.centi**3),ref="Kurnosov2007")

        # Quenching

        self.add("N2a + N2a -> N2v1 + N2+ + e",

                 ch.Constant(2e-10*co.centi**3),ref="Starikovskaia2001")

        self.add("N2+Bv0 + N2 -> N2+ + N2",

                 ch.Constant(8.84e-10*co.centi**3),ref="Dielecce2010")

        self.add("N2+Bv1 + N2 -> N2+ + N2",

                 ch.Constant(16e-10*co.centi**3),ref="JollyPlain1983")

        self.add("N2+Bv2 + N2 -> N2+ + N2",

                 ch.Constant(25e-10*co.centi**3),ref="JollyPlain1983")

        self.add("N2+Bv0 + O2 -> N2+ + O2",

                 ch.Constant(10.45e-10*co.centi**3),ref="Dielecce2010")

        self.add("N2+Bv1 + O2 -> N2+ + O2",

                 ch.Constant(10.45e-10*co.centi**3),ref="Dielecce2010")

        self.add("N2+Bv2 + O2 -> N2+ + O2",

                 ch.Constant(10.45e-10*co.centi**3),ref="Dielecce2010")

        self.add("N2E + N2 -> N2 + N2",

                 ch.Constant(2.4e-11*co.centi**3),ref="...")

        self.add("N2E + O2 -> N2 + O2",

                 ch.Constant(7e-12*co.centi**3),ref="...")

        self.add("O5P + N2 -> O + N2",

                 ch.Constant(1.05e-9*co.centi**3),ref="Dagdigian1988")

        self.add("O5P + O2 -> O + O2",

                 ch.Constant(1.05e-9*co.centi**3),ref="Dagdigian1988")

        self.add("O3P + N2 -> O + N2",

                 ch.Constant(5.9e-10*co.centi**3),ref="Dagdigian1988")

        self.add("O3P + O2 -> O + O2",

                 ch.Constant(7.8e-10*co.centi**3),ref="Dagdigian1988")

        self.add("O3P + O2 -> O5P + O2",

                 ch.Constant(6e-11*co.centi**3),ref="Dagdigian1988")

        self.add("O3P + N2 -> O5P + N2",

                 ch.Constant(2e-11*co.centi**3),ref="Dagdigian1988")

        self.add("N2Ac + N2 -> N2 + N2",

                 ch.Constant(3.7e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv1 + N2 -> N2 + N2",

                 ch.Constant(3.4e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv2 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv3 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv4 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv5 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv6 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv7 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv8 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv9 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv10 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv11 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv12 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv13 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv14 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv15 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv16 + N2 -> N2 + N2",

                 ch.Constant(3.55e-16*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Ac + O2 -> N2 + O2",

                 ch.Constant(2e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv1 + O2 -> N2 + O2",

                 ch.Constant(3.8e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv2 + O2 -> N2 + O2",

                 ch.Constant(5e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv3 + O2 -> N2 + O2",

                 ch.Constant(5.5e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv4 + O2 -> N2 + O2",

                 ch.Constant(6e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv5 + O2 -> N2 + O2",

                 ch.Constant(5.7e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv6 + O2 -> N2 + O2",

                 ch.Constant(6.5e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv7 + O2 -> N2 + O2",

                 ch.Constant(7e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv8 + O2 -> N2 + O2",

                 ch.Constant(5e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv9 + O2 -> N2 + O2",

                 ch.Constant(6e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv10 + O2 -> N2 + O2",

                 ch.Constant(6.2e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv11 + O2 -> N2 + O2",

                 ch.Constant(6.4e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv12 + O2 -> N2 + O2",

                 ch.Constant(6.6e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv13 + O2 -> N2 + O2",

                 ch.Constant(6.8e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv14 + O2 -> N2 + O2",

                 ch.Constant(7e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv15 + O2 -> N2 + O2",

                 ch.Constant(7.5e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv16 + O2 -> N2 + O2",

                 ch.Constant(7.6e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2B + N2 -> N2 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv1 + N2 -> N2 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv2 + N2 -> N2 + N2",

                 ch.Constant(8e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv3 + N2 -> N2 + N2",

                 ch.Constant(3e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv4 + N2 -> N2 + N2",

                 ch.Constant(2.4e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv5 + N2 -> N2 + N2",

                 ch.Constant(2.6e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv6 + N2 -> N2 + N2",

                 ch.Constant(2.1e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2B + O2 -> N2 + O2",

                 ch.Constant(2e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv1 + O2 -> N2 + O2",

                 ch.Constant(3.8e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv2 + O2 -> N2 + O2",

                 ch.Constant(8e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv3 + O2 -> N2 + O2",

                 ch.Constant(3e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv4 + O2 -> N2 + O2",

                 ch.Constant(2.4e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv5 + O2 -> N2 + O2",

                 ch.Constant(2.6e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv6 + O2 -> N2 + O2",

                 ch.Constant(2.1e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2C + N2 -> N2 + N2",

                 ch.Constant(1.09e-11*co.centi**3),ref="Simek2002")

        self.add("N2Cv1 + N2 -> N2 + N2",

                 ch.Constant(2.9e-11*co.centi**3),ref="Simek2002")

        self.add("N2Cv2 + N2 -> N2 + N2",

                 ch.Constant(4.3e-11*co.centi**3),ref="Simek2002")

        self.add("N2Cv3 + N2 -> N2 + N2",

                 ch.Constant(6.34e-11*co.centi**3),ref="Simek2002")

        self.add("N2Cv4 + N2 -> N2 + N2",

                 ch.Constant(9.86e-11*co.centi**3),ref="Simek2002")

        self.add("N2C + O2 -> N2 + O2",

                 ch.Constant(3e-10*co.centi**3),ref="Simek2002")

        self.add("N2Cv1 + O2 -> N2 + O2",

                 ch.Constant(3.1e-10*co.centi**3),ref="Simek2002")

        self.add("N2Cv2 + O2 -> N2 + O2",

                 ch.Constant(3.7e-10*co.centi**3),ref="Simek2002")

        self.add("N2Cv3 + O2 -> N2 + O2",

                 ch.Constant(4.3e-10*co.centi**3),ref="Simek2002")

        self.add("N2Cv4 + O2 -> N2 + O2",

                 ch.Constant(4.3e-10*co.centi**3),ref="Simek2002")

        self.add("N2a + N2 -> N2 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Piper1988")

        self.add("N2av1 + N2 -> N2 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Piper1988")

        self.add("N2av2 + N2 -> N2 + N2",

                 ch.Constant(8e-12*co.centi**3),ref="Piper1988")

        self.add("N2av3 + N2 -> N2 + N2",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1988")

        self.add("N2av4 + N2 -> N2 + N2",

                 ch.Constant(2.4e-11*co.centi**3),ref="Piper1988")

        self.add("N2av5 + N2 -> N2 + N2",

                 ch.Constant(2.6e-11*co.centi**3),ref="Piper1988")

        self.add("N2av6 + N2 -> N2 + N2",

                 ch.Constant(2.1e-11*co.centi**3),ref="Piper1988")

        self.add("N2av7 + N2 -> N2 + N2",

                 ch.Constant(6.4e-11*co.centi**3),ref="Piper1988")

        self.add("N2av8 + N2 -> N2 + N2",

                 ch.Constant(8e-11*co.centi**3),ref="Piper1988")

        self.add("N2av9 + N2 -> N2 + N2",

                 ch.Constant(7e-11*co.centi**3),ref="Piper1988")

        self.add("N2av10 + N2 -> N2 + N2",

                 ch.Constant(2.3e-11*co.centi**3),ref="Piper1988")

        self.add("N2av11 + N2 -> N2 + N2",

                 ch.Constant(2.8e-11*co.centi**3),ref="Piper1988")

        self.add("N2av12 + N2 -> N2 + N2",

                 ch.Constant(2.2e-11*co.centi**3),ref="Piper1988")


        self.add("N2a + O2 -> N2 + O2",

                 ch.Constant(1.5e-10*co.centi**3),ref="Piper1988")

        self.add("N2av1 + O2 -> N2 + O2",

                 ch.Constant(1e-11*co.centi**3),ref="Piper1988")

        self.add("N2av2 + O2 -> N2 + O2",

                 ch.Constant(1.5e-10*co.centi**3),ref="Piper1988")

        self.add("N2av3 + O2 -> N2 + O2",

                 ch.Constant(1.5e-10*co.centi**3),ref="Piper1988")

        self.add("N2av4 + O2 -> N2 + O2",

                 ch.Constant(1.7e-10*co.centi**3),ref="Piper1988")

        self.add("N2av5 + O2 -> N2 + O2",

                 ch.Constant(1.6e-10*co.centi**3),ref="Piper1988")

        self.add("N2av6 + O2 -> N2 + O2",

                 ch.Constant(2.2e-10*co.centi**3),ref="Piper1988")

        self.add("N2av7 + O2 -> N2 + O2",

                 ch.Constant(2.3e-10*co.centi**3),ref="Piper1988")

        self.add("N2av8 + O2 -> N2 + O2",

                 ch.Constant(2.7e-10*co.centi**3),ref="Piper1988")

        self.add("N2av9 + O2 -> N2 + O2",

                 ch.Constant(1.9e-10*co.centi**3),ref="Piper1988")

        self.add("N2av10 + O2 -> N2 + O2",

                 ch.Constant(4e-10*co.centi**3),ref="Piper1988")

        self.add("N2av11 + O2 -> N2 + O2",

                 ch.Constant(3.2e-10*co.centi**3),ref="Piper1988")

        self.add("N2av12 + O2-> N2 + O2",

                 ch.Constant(2e-11*co.centi**3),ref="Piper1988")

        self.add("N2Bp + N2 -> N2 + N2",

                 ch.Constant(2e-13*co.centi**3),ref="Piper1989")

        self.add("N2Bpv1 + N2 -> N2 + N2",

                 ch.Constant(3e-13*co.centi**3),ref="Piper1989")

        self.add("N2Wc + N2 -> N2 + N2",

                 ch.Constant(1.65e-12*co.centi**3),ref="Piper1989")

        self.add("N2Wcv1 + N2 -> N2 + N2",

                 ch.Constant(2.4e-12*co.centi**3),ref="Piper1989")

        self.add("N2Wcv2 + N2 -> N2 + N2",

                 ch.Constant(3e-12*co.centi**3),ref="Piper1989")

        self.add("N2Wcv3 + N2 -> N2 + N2",

                 ch.Constant(7.5e-12*co.centi**3),ref="Piper1989")

        # Vibrational redistribution
        self.add("N2Cv4 + N2 -> N2Cv3 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Calo1971")

        self.add("N2Cv3 + N2 -> N2Cv2 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Calo1971")

        self.add("N2Cv2 + N2 -> N2Cv1 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Calo1971")

        self.add("N2Cv1 + N2 -> N2C + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Calo1971")

        self.add("N2Acv2 + N2 -> N2Ac + N2v1",

                 ch.Constant(7.5e-15*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv3 + N2 -> N2Acv1 + N2v1",

                 ch.Constant(5.7e-14*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv4 + N2 -> N2Acv2 + N2v1",

                 ch.Constant(1.25e-13*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv5 + N2 -> N2Acv3 + N2v1",

                 ch.Constant(4.8e-13*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv6 + N2 -> N2Acv4 + N2v1",

                 ch.Constant(7.5e-13*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv7 + N2 -> N2Acv5 + N2v1",

                 ch.Constant(8.75e-13*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv8 + N2 -> N2Acv6 + N2v1",

                 ch.Constant(1e-14*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv9 + N2 -> N2Acv7 + N2v1",

                 ch.Constant(1.1e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv10 + N2 -> N2Acv8 + N2v1",

                 ch.Constant(1.5e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv11 + N2 -> N2Acv9 + N2v1",

                 ch.Constant(1.1e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv12 + N2 -> N2Acv10 + N2v1",

                 ch.Constant(1.1e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv13 + N2 -> N2Acv11 + N2v1",

                 ch.Constant(1e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv14 + N2 -> N2Acv12 + N2v1",

                 ch.Constant(9.5e-13*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv15 + N2 -> N2Acv13 + N2v1",

                 ch.Constant(7.5e-13*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv16 + N2 -> N2Acv14 + N2v1",

                 ch.Constant(4.5e-13*co.centi**3),ref="MorrilBenesch1996")



        # Intersystem collisional transfer

        self.add("N2Ac + N2Ac -> N2Cv1 + N2",

                 ch.Constant(4.1e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Cv2 + N2",

                 ch.Constant(4.1e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Cv3 + N2",

                 ch.Constant(2.8e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Cv4 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2C + N2",

                 ch.Constant(3.4e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Cv1 + N2",

                 ch.Constant(5.4e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Cv2 + N2",

                 ch.Constant(3.3e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Cv3 + N2",

                 ch.Constant(2.2e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Cv4 + N2",

                 ch.Constant(1e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Bv1 + N2",

                 ch.Constant(2.4e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Bv2 + N2",

                 ch.Constant(3.9e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Bv3 + N2",

                 ch.Constant(1.9e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Bv4 + N2",

                 ch.Constant(1.6e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Bv5 + N2",

                 ch.Constant(1.5e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Ac -> N2Bv6 + N2",

                 ch.Constant(1.1e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Bv1 + N2",

                 ch.Constant(6.6e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Bv2 + N2",

                 ch.Constant(5.6e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Bv3 + N2",

                 ch.Constant(5.8e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Bv4 + N2",

                 ch.Constant(2.7e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Bv5 + N2",

                 ch.Constant(2.2e-11*co.centi**3),ref="Piper1988")

        self.add("N2Ac + N2Acv1 -> N2Bv6 + N2",

                 ch.Constant(1.7e-11*co.centi**3),ref="Piper1988")

        self.add("N2B + N2 -> N2Acv7 + N2",

                 ch.Constant(7.83e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv1 + N2 -> N2Acv8 + N2",

                 ch.Constant(6.14e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv1 + N2 -> N2Acv9 + N2",

                 ch.Constant(5.21e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv2 + N2 -> N2Acv10 + N2",

                 ch.Constant(2.2e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv3 + N2 -> N2Acv11 + N2",

                 ch.Constant(8.57e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv3 + N2 -> N2Acv12 + N2",

                 ch.Constant(2.78e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv4 + N2 -> N2Acv12 + N2",

                 ch.Constant(9.5e-13*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv4 + N2 -> N2Acv13 + N2",

                 ch.Constant(6e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv5 + N2 -> N2Acv14 + N2",

                 ch.Constant(8.52e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv5 + N2 -> N2Acv15 + N2",

                 ch.Constant(2.69e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv6 + N2 -> N2Acv15 + N2",

                 ch.Constant(3.68e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Bv6 + N2 -> N2Acv16 + N2",

                 ch.Constant(2.81e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv7 + N2 -> N2B + N2",

                 ch.Constant(4.41e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv8 + N2 -> N2Bv1 + N2",

                 ch.Constant(3.45e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv9 + N2 -> N2Bv1 + N2",

                 ch.Constant(2.93e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv10 + N2 -> N2Bv2 + N2",

                 ch.Constant(1.24e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv11 + N2 -> N2Bv3 + N2",

                 ch.Constant(4.85e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv12 + N2 -> N2Bv3 + N2",

                 ch.Constant(1.56e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv12 + N2 -> N2Bv4 + N2",

                 ch.Constant(5.34e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv13 + N2 -> N2Bv4 + N2",

                 ch.Constant(3.38e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv14 + N2 -> N2Bv5 + N2",

                 ch.Constant(4.79e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv15 + N2 -> N2Bv5 + N2",

                 ch.Constant(1.51e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv15 + N2 -> N2Bv3 + N2",

                 ch.Constant(2.07e-12*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Acv16 + N2 -> N2Bv6 + N2",

                 ch.Constant(1.58e-11*co.centi**3),ref="MorrilBenesch1996")

        self.add("N2Ac + N2v5 -> N2Bv1 + N2",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v6 -> N2Bv1 + N2v1",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v7 -> N2Bv1 + N2v2",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v8 -> N2Bv1 + N2v3",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v9 -> N2Bv1 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v10 -> N2Bv1 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v6 -> N2Bv2 + N2v1",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v7 -> N2Bv2 + N2v2",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v8 -> N2Bv2 + N2v3",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v9 -> N2Bv2 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v10 -> N2Bv2 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v7 -> N2Bv3 + N2v2",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v8 -> N2Bv3 + N2v3",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v9 -> N2Bv3 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v10 -> N2Bv3 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v8 -> N2Bv4 + N2v3",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v9 -> N2Bv4 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v10 -> N2Bv4 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v9 -> N2Bv5 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v10 -> N2Bv5 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Ac + N2v10 -> N2Bv6 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v5 -> N2Bv1 + N2",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v6 -> N2Bv1 + N2v1",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v7 -> N2Bv1 + N2v2",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v8 -> N2Bv1 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v9 -> N2Bv1 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v10 -> N2Bv1 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v6 -> N2Bv2 + N2v1",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v7 -> N2Bv2 + N2v1",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v8 -> N2Bv2 + N2v3",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v9 -> N2Bv2 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v10 -> N2Bv2 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v7 -> N2Bv3 + N2v2",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v8 -> N2Bv3 + N2v3",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v9 -> N2Bv3 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v10 -> N2Bv3 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v9 -> N2Bv4 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v10 -> N2Bv4 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v9 -> N2Bv5 + N2v4",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v10 -> N2Bv5 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2Acv1 + N2v10 -> N2Bv6 + N2v5",

                 ch.Constant(3e-11*co.centi**3),ref="Piper1989")

        self.add("N2B + N2 -> N2Wc + N2",

                 ch.Constant(1.72e-11*co.centi**3),ref="Piper1989")

        self.add("N2Wc + N2 -> N2B + N2",

                 ch.Constant(1.28e-11*co.centi**3),ref="Piper1989")

        self.add("N2Bv1 + N2 -> N2Wcv1 + N2",

                 ch.Constant(4.17e-11*co.centi**3),ref="Piper1989")

        self.add("N2Wcv1 + N2 -> N2Bv1 + N2",

                 ch.Constant(3.09e-11*co.centi**3),ref="Piper1989")

        self.add("N2Bv2 + N2 -> N2Wcv2 + N2",

                 ch.Constant(2.27e-11*co.centi**3),ref="Piper1989")

        self.add("N2Wcv2 + N2 -> N2Bv2 + N2",

                 ch.Constant(1.68e-11*co.centi**3),ref="Piper1989")

        self.add("N2Bv4 + N2 -> N2Bp + N2",

                 ch.Constant(8.49e-12*co.centi**3),ref="Piper1989")

        self.add("N2Bp + N2 -> N2Bv4 + N2",

                 ch.Constant(4.72e-12*co.centi**3),ref="Piper1989")

        self.add("N2Bv5 + N2 -> N2Bpv1 + N2",

                 ch.Constant(1.17e-11*co.centi**3),ref="Piper1989")

        self.add("N2Bpv1 + N2 -> N2Bv5 + N2",

                 ch.Constant(6.5e-12*co.centi**3),ref="Piper1989")

        # Radiative decay

        self.add("N2Ac -> N2",

                 ch.Constant(5e-1),ref="Capitelli2000")

        self.add("N2Wc -> N2",

                 ch.Constant(1.54e-1),ref="Capitelli2000")

        self.add("N2Bp -> N2B",

                 ch.Constant(3.4e4),ref="Capitelli2000")

        self.add("N2E -> N2Ac",

                 ch.Constant(1.2e3),ref="Capitelli2000")

        self.add("N2E -> N2B",

                 ch.Constant(3.46e2),ref="Capitelli2000")

        self.add("N2w -> N2a",

                 ch.Constant(1.51e3),ref="Capitelli2000")

        self.add("N2ap -> N2",

                 ch.Constant(1e2),ref="Capitelli2000")

        self.add("N2app -> N2",

                 ch.Constant(2.86e5),ref="Capitelli2000")

        self.add("N2B -> N2Ac",

                 ch.Constant(5.09e4),ref="Gilmore1992")

        self.add("N2B -> N2Acv1",

                 ch.Constant(2.73e4),ref="Gilmore1992")

        self.add("N2B -> N2Acv2",

                 ch.Constant(8.14e3),ref="Gilmore1992")

        self.add("N2B -> N2Acv3",

                 ch.Constant(1.72e3),ref="Gilmore1992")

        self.add("N2B -> N2Acv4",

                 ch.Constant(2.66e3),ref="Gilmore1992")

        self.add("N2Bv1 -> N2Ac",

                 ch.Constant(7.7e4),ref="Gilmore1992")

        self.add("N2Bv1 -> N2Acv2",

                 ch.Constant(1.44e4),ref="Gilmore1992")

        self.add("N2Bv1 -> N2Acv3",

                 ch.Constant(1.09e4),ref="Gilmore1992")

        self.add("N2Bv1 -> N2Acv1",

                 ch.Constant(3.6e2),ref="Gilmore1992")

        self.add("N2Bv1 -> N2Acv4",

                 ch.Constant(4e3),ref="Gilmore1992")

        self.add("N2Bv1 -> N2Acv5",

                 ch.Constant(9.46e2),ref="Gilmore1992")

        self.add("N2Bv1 -> N2Acv6",

                 ch.Constant(1.48e2),ref="Gilmore1992")

        self.add("N2Bv2 -> N2Ac",

                 ch.Constant(4.28e4),ref="Gilmore1992")

        self.add("N2Bv2 -> N2Acv1",

                 ch.Constant(5.55e4),ref="Gilmore1992")

        self.add("N2Bv2 -> N2Acv2",

                 ch.Constant(1.03e4),ref="Gilmore1992")

        self.add("N2Bv2 -> N2Acv3",

                 ch.Constant(2.13e3),ref="Gilmore1992")

        self.add("N2Bv2 -> N2Acv4",

                 ch.Constant(7.82e3),ref="Gilmore1992")

        self.add("N2Bv2 -> N2Acv5",

                 ch.Constant(5.19e3),ref="Gilmore1992")

        self.add("N2Bv2 -> N2Acv6",

                 ch.Constant(1.85e3),ref="Gilmore1992")

        self.add("N2Bv2 -> N2Acv7",

                 ch.Constant(4.2e2),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Ac",

                 ch.Constant(1.16e4),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv1",

                 ch.Constant(7.61e4),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv2",

                 ch.Constant(2e4),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv3",

                 ch.Constant(2.39e4),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv4",

                 ch.Constant(2.69e3),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv5",

                 ch.Constant(4.58e3),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv6",

                 ch.Constant(2.54e3),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv7",

                 ch.Constant(6.02e2),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv8",

                 ch.Constant(8.26e2),ref="Gilmore1992")

        self.add("N2Bv3 -> N2Acv9",

                 ch.Constant(1.65e2),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Ac",

                 ch.Constant(1.62e3),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv1",

                 ch.Constant(3.34e4),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv2",

                 ch.Constant(8.41e4),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv3",

                 ch.Constant(1.54e3),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv4",

                 ch.Constant(2.54e4),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv5",

                 ch.Constant(6.2e3),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv6",

                 ch.Constant(2.69e3),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv7",

                 ch.Constant(1.59e2),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv8",

                 ch.Constant(2.62e3),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv9",

                 ch.Constant(1.23e3),ref="Gilmore1992")

        self.add("N2Bv4 -> N2Acv10",

                 ch.Constant(3.53e2),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv1",

                 ch.Constant(6.57e3),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv2",

                 ch.Constant(5.93e4),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv3",

                 ch.Constant(7.05e4),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv4",

                 ch.Constant(2.62e3),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv5",

                 ch.Constant(1.68e4),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv6",

                 ch.Constant(1.23e4),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Ac",

                 ch.Constant(1.19e2),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv7",

                 ch.Constant(8.15e2),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv8",

                 ch.Constant(7.74e2),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv9",

                 ch.Constant(2e3),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv10",

                 ch.Constant(1.45e3),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv11",

                 ch.Constant(5.87e2),ref="Gilmore1992")

        self.add("N2Bv5 -> N2Acv12",

                 ch.Constant(1.43e2),ref="Gilmore1992")

        self.add("N2Bv6 -> N2Acv2",

                 ch.Constant(1.58e4),ref="Gilmore1992")

        self.add("N2Bv6 -> N2Acv3",

                 ch.Constant(8.19e4),ref="Gilmore1992")

        self.add("N2Bv6 -> N2Acv4",

                 ch.Constant(4.6e4),ref="Gilmore1992")
        self.add("N2Bv6 -> N2Acv5",

                 ch.Constant(1.45e4),ref="Gilmore1992")
        self.add("N2Bv6 -> N2Acv6",

                 ch.Constant(6.46e3),ref="Gilmore1992")
        self.add("N2Bv6 -> N2Acv7",

                 ch.Constant(1.46e4),ref="Gilmore1992")
        self.add("N2Bv6 -> N2Acv8",

                 ch.Constant(3.95e3),ref="Gilmore1992")
        self.add("N2Bv6 -> N2Acv11",

                 ch.Constant(1.32e3),ref="Gilmore1992")

        self.add("N2Bv6 -> N2Acv1",

                 ch.Constant(6.2e2),ref="Gilmore1992")

        self.add("N2Bv6 -> N2Acv10",

                 ch.Constant(9.86e2),ref="Gilmore1992")
        self.add("N2Bv6 -> N2Acv12",

                 ch.Constant(7.78e2),ref="Gilmore1992")
        self.add("N2Bv6 -> N2Acv13",

                 ch.Constant(2.73e2),ref="Gilmore1992")

        self.add("N2C -> N2B",

                 ch.Constant(1.32e7),ref="Gilmore1992")

        self.add("N2C -> N2Bv1",

                 ch.Constant(8.84e6),ref="Gilmore1992")

        self.add("N2C -> N2Bv2",

                 ch.Constant(3.53e6),ref="Gilmore1992")

        self.add("N2C -> N2Bv3",

                 ch.Constant(1.09e6),ref="Gilmore1992")

        self.add("N2C -> N2Bv4",

                 ch.Constant(2.86e5),ref="Gilmore1992")

        self.add("N2C -> N2Bv5",

                 ch.Constant(6.78e4),ref="Gilmore1992")

        self.add("N2C -> N2Bv6",

                 ch.Constant(1.5e4),ref="Gilmore1992")

        self.add("N2Cv1 -> N2B",

                 ch.Constant(1.19e7),ref="Gilmore1992")

        self.add("N2Cv1 -> N2Bv1",

                 ch.Constant(6.19e5),ref="Gilmore1992")

        self.add("N2Cv1 -> N2Bv2",

                 ch.Constant(5.6e6),ref="Gilmore1992")

        self.add("N2Cv1 -> N2Bv3",

                 ch.Constant(4.93e6),ref="Gilmore1992")

        self.add("N2Cv1 -> N2Bv4",

                 ch.Constant(2.41e6),ref="Gilmore1992")

        self.add("N2Cv1 -> N2Bv5",

                 ch.Constant(8.84e5),ref="Gilmore1992")

        self.add("N2Cv1 -> N2Bv6",

                 ch.Constant(2.72e5),ref="Gilmore1992")

        self.add("N2Cv2 -> N2B",

                 ch.Constant(3.94e6),ref="Gilmore1992")

        self.add("N2Cv2 -> N2Bv1",

                 ch.Constant(1.02e7),ref="Gilmore1992")

        self.add("N2Cv2 -> N2Bv2",

                 ch.Constant(7.58e5),ref="Gilmore1992")

        self.add("N2Cv2 -> N2Bv3",

                 ch.Constant(1.76e6),ref="Gilmore1992")

        self.add("N2Cv2 -> N2Bv4",

                 ch.Constant(4.07e6),ref="Gilmore1992")

        self.add("N2Cv2 -> N2Bv5",

                 ch.Constant(3.13e6),ref="Gilmore1992")

        self.add("N2Cv2 -> N2Bv6",

                 ch.Constant(1.55e6),ref="Gilmore1992")

        self.add("N2Cv3 -> N2B",

                 ch.Constant(5.23e5),ref="Gilmore1992")

        self.add("N2Cv3 -> N2Bv1",

                 ch.Constant(7.25e6),ref="Gilmore1992")
        self.add("N2Cv3 -> N2Bv2",

                 ch.Constant(6.01e6),ref="Gilmore1992")
        self.add("N2Cv3 -> N2Bv3",

                 ch.Constant(2.79e6),ref="Gilmore1992")

        self.add("N2Cv3 -> N2Bv4",

                 ch.Constant(1.15e5),ref="Gilmore1992")

        self.add("N2Cv3 -> N2Bv5",

                 ch.Constant(2.4e6),ref="Gilmore1992")

        self.add("N2Cv3 -> N2Bv6",

                 ch.Constant(3.02e6),ref="Gilmore1992")
        self.add("N2Cv4 -> N2B",

                 ch.Constant(1.44e4),ref="Gilmore1992")

        self.add("N2Cv4 -> N2Bv1",

                 ch.Constant(1.29e6),ref="Gilmore1992")

        self.add("N2Cv4 -> N2Bv2",

                 ch.Constant(8.99e6),ref="Gilmore1992")

        self.add("N2Cv4 -> N2Bv3",

                 ch.Constant(3.09e6),ref="Gilmore1992")

        self.add("N2Cv4 -> N2Bv4",

                 ch.Constant(3.66e6),ref="Gilmore1992")

        self.add("N2Cv4 -> N2Bv5",

                 ch.Constant(1.24e5),ref="Gilmore1992")

        self.add("N2Cv4 -> N2Bv6",

                 ch.Constant(1.04e6),ref="Gilmore1992")

        self.add("N2a -> N2ap",

                 ch.Constant(9.74e1),ref="Gilmore1992")

        self.add("N2av1 -> N2ap",

                 ch.Constant(5.98e2),ref="Gilmore1992")

        self.add("N2av2 -> N2ap",

                 ch.Constant(3.51e2),ref="Gilmore1992")

        self.add("N2av3 -> N2ap",

                 ch.Constant(5.03e1),ref="Gilmore1992")

        self.add("N2a -> N2",

                 ch.Constant(9.93e2),ref="Gilmore1992")

        self.add("N2a -> N2",

                 ch.Constant(9.93e2),ref="Gilmore1992")

        self.add("N2a -> N2v1",

                 ch.Constant(3.14e3),ref="Gilmore1992")

        self.add("N2a -> N2v2",

                 ch.Constant(4.63e3),ref="Gilmore1992")

        self.add("N2a -> N2v3",

                 ch.Constant(4.19e3),ref="Gilmore1992")

        self.add("N2a -> N2v4",

                 ch.Constant(2.6e3),ref="Gilmore1992")

        self.add("N2a -> N2v5",

                 ch.Constant(1.17e3),ref="Gilmore1992")

        self.add("N2a -> N2v6",

                 ch.Constant(3.99e2),ref="Gilmore1992")

        self.add("N2a -> N2v7",

                 ch.Constant(1.04e2),ref="Gilmore1992")

        self.add("N2av1 -> N2",

                 ch.Constant(2.85e3),ref="Gilmore1992")

        self.add("N2av1 -> N2v1",

                 ch.Constant(4.31e3),ref="Gilmore1992")

        self.add("N2av1 -> N2v2",

                 ch.Constant(1.63e3),ref="Gilmore1992")

        self.add("N2av1 -> N2v4",

                 ch.Constant(1.44e3),ref="Gilmore1992")

        self.add("N2av1 -> N2v5",

                 ch.Constant(2.72e3),ref="Gilmore1992")

        self.add("N2av1 -> N2v6",

                 ch.Constant(2.28e3),ref="Gilmore1992")

        self.add("N2av1 -> N2v7",

                 ch.Constant(1.19e3),ref="Gilmore1992")

        self.add("N2av1 -> N2v8",

                 ch.Constant(4.27e2),ref="Gilmore1992")

        self.add("N2av2 -> N2",

                 ch.Constant(4.5e3),ref="Gilmore1992")

        self.add("N2av2 -> N2v1",

                 ch.Constant(2.33e3),ref="Gilmore1992")

        self.add("N2av2 -> N2v3",

                 ch.Constant(2.1e3),ref="Gilmore1992")

        self.add("N2av2 -> N2v4",

                 ch.Constant(1.51e3),ref="Gilmore1992")

        self.add("N2av2 -> N2v6",

                 ch.Constant(9.47e2),ref="Gilmore1992")

        self.add("N2av2 -> N2v7",

                 ch.Constant(2.11e3),ref="Gilmore1992")

        self.add("N2av2 -> N2v8",

                 ch.Constant(1.82e3),ref="Gilmore1992")

        self.add("N2av3 -> N2",

                 ch.Constant(5.16e3),ref="Gilmore1992")

        self.add("N2av3 -> N2v1",

                 ch.Constant(3.22e2),ref="Gilmore1992")

        self.add("N2av3 -> N2v2",

                 ch.Constant(1.74e3),ref="Gilmore1992")

        self.add("N2av3 -> N2v3",

                 ch.Constant(1.46e3),ref="Gilmore1992")

        self.add("N2av3 -> N2v5",

                 ch.Constant(1.64e3),ref="Gilmore1992")

        self.add("N2av3 -> N2v6",

                 ch.Constant(9.96e2),ref="Gilmore1992")

        self.add("N2av3 -> N2v7",

                 ch.Constant(1.02e3),ref="Gilmore1992")

        self.add("N2av4 -> N2",

                 ch.Constant(4.82e3),ref="Gilmore1992")

        self.add("N2av4 -> N2v1",

                 ch.Constant(1.65e2),ref="Gilmore1992")

        self.add("N2av4 -> N2v2",

                 ch.Constant(2.4e3),ref="Gilmore1992")

        self.add("N2av4 -> N2v4",

                 ch.Constant(1.58e3),ref="Gilmore1992")

        self.add("N2av4 -> N2v5",

                 ch.Constant(6.79e2),ref="Gilmore1992")

        self.add("N2av4 -> N2v6",

                 ch.Constant(2.96e2),ref="Gilmore1992")

        self.add("N2av4 -> N2v7",

                 ch.Constant(1.45e3),ref="Gilmore1992")

        self.add("N2av4 -> N2v8",

                 ch.Constant(4.57e2),ref="Gilmore1992")

        self.add("N2av5 -> N2",

                 ch.Constant(3.9e3),ref="Gilmore1992")

        self.add("N2av5 -> N2v1",

                 ch.Constant(1.34e3),ref="Gilmore1992")

        self.add("N2av5 -> N2v2",

                 ch.Constant(1.25e3),ref="Gilmore1992")

        self.add("N2av5 -> N2v3",

                 ch.Constant(8.1e2),ref="Gilmore1992")

        self.add("N2av5 -> N2v4",

                 ch.Constant(1.24e3),ref="Gilmore1992")

        self.add("N2av5 -> N2v5",

                 ch.Constant(1.71e2),ref="Gilmore1992")

        self.add("N2av5 -> N2v6",

                 ch.Constant(1.41e3),ref="Gilmore1992")

        self.add("N2av5 -> N2v7",

                 ch.Constant(1.15e2),ref="Gilmore1992")

        self.add("N2av5 -> N2v8",

                 ch.Constant(7.14e2),ref="Gilmore1992")

        self.add("N2av6 -> N2",

                 ch.Constant(2.83e3),ref="Gilmore1992")

        self.add("N2av6 -> N2v1",

                 ch.Constant(2.62e3),ref="Gilmore1992")

        self.add("N2av6 -> N2v2",

                 ch.Constant(1.36e2),ref="Gilmore1992")

        self.add("N2av6 -> N2v3",

                 ch.Constant(1.87e3),ref="Gilmore1992")

        self.add("N2av6 -> N2v5",

                 ch.Constant(1.35e3),ref="Gilmore1992")

        self.add("N2av6 -> N2v6",

                 ch.Constant(2.74e2),ref="Gilmore1992")

        self.add("N2av6 -> N2v7",

                 ch.Constant(7.25e2),ref="Gilmore1992")

        self.add("N2av6 -> N2v8",

                 ch.Constant(8.4e2),ref="Gilmore1992")

        self.add("N2av7 -> N2",

                 ch.Constant(1.9e3),ref="Gilmore1992")

        self.add("N2av7 -> N2v1",

                 ch.Constant(3.26e3),ref="Gilmore1992")

        self.add("N2av7 -> N2v2",

                 ch.Constant(1.64e2),ref="Gilmore1992")

        self.add("N2av7 -> N2v3",

                 ch.Constant(1.55e3),ref="Gilmore1992")

        self.add("N2av7 -> N2v4",

                 ch.Constant(4.26e2),ref="Gilmore1992")

        self.add("N2av7 -> N2v5",

                 ch.Constant(1.06e3),ref="Gilmore1992")

        self.add("N2av7 -> N2v6",

                 ch.Constant(2.73e2),ref="Gilmore1992")

        self.add("N2av7 -> N2v7",

                 ch.Constant(1.06e3),ref="Gilmore1992")

        self.add("N2av8 -> N2",

                 ch.Constant(1.2e3),ref="Gilmore1992")

        self.add("N2av8 -> N2v1",

                 ch.Constant(3.2e3),ref="Gilmore1992")

        self.add("N2av8 -> N2v2",

                 ch.Constant(1.05e3),ref="Gilmore1992")

        self.add("N2av8 -> N2v3",

                 ch.Constant(5.4e2),ref="Gilmore1992")

        self.add("N2av8 -> N2v4",

                 ch.Constant(1.41e3),ref="Gilmore1992")

        self.add("N2av8 -> N2v6",

                 ch.Constant(1.2e3),ref="Gilmore1992")

        self.add("N2av8 -> N2v8",

                 ch.Constant(9.29e2),ref="Gilmore1992")

        self.add("N2av9 -> N2",

                 ch.Constant(7.17e2),ref="Gilmore1992")

        self.add("N2av9 -> N2v1",

                 ch.Constant(2.71e3),ref="Gilmore1992")

        self.add("N2av9 -> N2v2",

                 ch.Constant(2.03e3),ref="Gilmore1992")

        self.add("N2av9 -> N2v4",

                 ch.Constant(1.54e3),ref="Gilmore1992")

        self.add("N2av9 -> N2v5",

                 ch.Constant(2.39e2),ref="Gilmore1992")

        self.add("N2av9 -> N2v6",

                 ch.Constant(9.2e2),ref="Gilmore1992")

        self.add("N2av9 -> N2v7",

                 ch.Constant(3.53e2),ref="Gilmore1992")

        self.add("N2av9 -> N2v8",

                 ch.Constant(7.28e2),ref="Gilmore1992")

        self.add("N2av10 -> N2",

                 ch.Constant(4.14e2),ref="Gilmore1992")

        self.add("N2av10 -> N2v1",

                 ch.Constant(2.06e3),ref="Gilmore1992")

        self.add("N2av10 -> N2v2",

                 ch.Constant(2.61e3),ref="Gilmore1992")

        self.add("N2av10 -> N2v3",

                 ch.Constant(3.07e2),ref="Gilmore1992")

        self.add("N2av10 -> N2v4",

                 ch.Constant(8.37e2),ref="Gilmore1992")

        self.add("N2av10 -> N2v5",

                 ch.Constant(1.07e3),ref="Gilmore1992")

        self.add("N2av10 -> N2v6",

                 ch.Constant(1.21e2),ref="Gilmore1992")

        self.add("N2av10 -> N2v7",

                 ch.Constant(1.08e3),ref="Gilmore1992")

        self.add("N2av11 -> N2",

                 ch.Constant(2.32e2),ref="Gilmore1992")

        self.add("N2av11 -> N2v1",

                 ch.Constant(1.45e3),ref="Gilmore1992")

        self.add("N2av11 -> N2v2",

                 ch.Constant(2.68e3),ref="Gilmore1992")

        self.add("N2av11 -> N2v3",

                 ch.Constant(1.08e3),ref="Gilmore1992")

        self.add("N2av11 -> N2v4",

                 ch.Constant(1.46e2),ref="Gilmore1992")

        self.add("N2av11 -> N2v5",

                 ch.Constant(1.42e3),ref="Gilmore1992")

        self.add("N2av11 -> N2v6",

                 ch.Constant(1.35e2),ref="Gilmore1992")

        self.add("N2av11 -> N2v7",

                 ch.Constant(8.16e2),ref="Gilmore1992")

        self.add("N2av11 -> N2v8",

                 ch.Constant(4.04e2),ref="Gilmore1992")

        self.add("N2av12 -> N2",

                 ch.Constant(1.27e2),ref="Gilmore1992")

        self.add("N2av12 -> N2v1",

                 ch.Constant(9.64e2),ref="Gilmore1992")

        self.add("N2av12 -> N2v3",

                 ch.Constant(2.38e3),ref="Gilmore1992")

        self.add("N2av12 -> N2v4",

                 ch.Constant(1.83e3),ref="Gilmore1992")

        self.add("N2av12 -> N2v5",

                 ch.Constant(1e3),ref="Gilmore1992")

        self.add("N2av12 -> N2v6",

                 ch.Constant(8.06e2),ref="Gilmore1992")

        self.add("N2av12 -> N2v7",

                 ch.Constant(1.38e2),ref="Gilmore1992")

        self.add("N2av12 -> N2v8",

                 ch.Constant(9.78e2),ref="Gilmore1992")

        self.add("N2av13 -> N2v1",

                 ch.Constant(6.13e2),ref="Gilmore1992")

        self.add("N2av13 -> N2v2",

                 ch.Constant(1.92e3),ref="Gilmore1992")

        self.add("N2av13 -> N2v3",

                 ch.Constant(2.25e3),ref="Gilmore1992")

        self.add("N2av13 -> N2v4",

                 ch.Constant(4.82e2),ref="Gilmore1992")

        self.add("N2av13 -> N2v5",

                 ch.Constant(3.49e2),ref="Gilmore1992")

        self.add("N2av13 -> N2v6",

                 ch.Constant(1.25e3),ref="Gilmore1992")

        self.add("N2av13 -> N2v8",

                 ch.Constant(7.39e2),ref="Gilmore1992")

        self.add("N2av14 -> N2v1",

                 ch.Constant(3.77e2),ref="Gilmore1992")

        self.add("N2av14 -> N2v2",

                 ch.Constant(1.43e3),ref="Gilmore1992")

        self.add("N2av14 -> N2v3",

                 ch.Constant(2.31e3),ref="Gilmore1992")

        self.add("N2av14 -> N2v4",

                 ch.Constant(1.14e3),ref="Gilmore1992")

        self.add("N2av14 -> N2v6",

                 ch.Constant(1.07e3),ref="Gilmore1992")

        self.add("N2av14 -> N2v7",

                 ch.Constant(5.97e2),ref="Gilmore1992")

        self.add("N2av14 -> N2v8",

                 ch.Constant(1.58e2),ref="Gilmore1992")

        self.add("N2av15 -> N2v1",

                 ch.Constant(2.26e2),ref="Gilmore1992")

        self.add("N2av15 -> N2v2",

                 ch.Constant(1.01e3),ref="Gilmore1992")

        self.add("N2av15 -> N2v3",

                 ch.Constant(2.09e3),ref="Gilmore1992")

        self.add("N2av15 -> N2v4",

                 ch.Constant(1.7e3),ref="Gilmore1992")

        self.add("N2av15 -> N2v5",

                 ch.Constant(1.52e2),ref="Gilmore1992")

        self.add("N2av15 -> N2v6",

                 ch.Constant(5.38e2),ref="Gilmore1992")

        self.add("N2av15 -> N2v7",

                 ch.Constant(1.07e3),ref="Gilmore1992")

        self.add("N2+Bv0 -> N2+",

                 ch.Constant(1.14e7+3.71e6+7.84e5+3.35e5),ref="Gilmore1992")

        self.add("N2+Bv1 -> N2+",

                 ch.Constant(5.76e6+4.06e6+4.27e6+1.57e6+3.92e5),ref="Gilmore1992")

        self.add("N2+Bv2 -> N2+",

                 ch.Constant(9.02e5+7.88e6+9.27e5+3.47e6+2.04e6+6.93e5+1.8e5),ref="Gilmore1992")

        self.add("NOA -> NO",

                 ch.Constant(5e6),ref="RazigSmirnov2012")

        self.add("OS -> OD",

                 ch.Constant(1.34),ref="VallanceJones1974")

        self.add("OD -> O",

                 ch.Constant(5.1e-3),ref="VallanceJones1974")

        self.add("O3P -> O",

                 ch.Constant(2.98e7),ref="Dagdigian1988")

        self.add("O5P -> O",

                 ch.Constant(2.01e7),ref="Dagdigian1988")

        self.add("O2A -> O2",

                 ch.Constant(1.1e1),ref="SmirnovMassey1982")

        self.add("O2a -> O2",

                 ch.Constant(3.31e-4),ref="SlangerCopeland2003")

        self.add("O2b -> O2",

                 ch.Constant(8.2e-2),ref="ValanceJones1974")

        self.add("O2b -> O2a",

                 ch.Constant(1.7e-3),ref="Kuprenie1972")



        # Add bulk recombination reactions

        pos_recomb = ['N2', 'O2', 'N', 'O', 'NO', 'NO2', 'N2O'] #[s for s in self.species if '+' in s]

        neg_recomb = ['O2', 'O', 'O3', 'NO', 'NO2', 'NO3'] #[s for s in self.species if '-' in s]

        pos = [s for s in pos_recomb]
        neg = [s for s in neg_recomb]

        self.add_pattern("{pos}+ + {neg}- -> {pos} + {neg}",

                         {'pos': pos, 'neg': neg},

                         ch.Constant(2e-7 * co.centi**3),

                         generic="A+ + B- -> A + B",

                         ref="Kossyi1992/PSST")

        neutral_recomb = ['N2', 'O2', 'NO', 'N', 'O']
        ion_recomb = ['N2+', 'O2+', 'NO+', 'N+', 'O+']

        neu = [s for s in neutral_recomb]
        pos = [s for s in ion_recomb]

        self.add_pattern("e + e + {pos} -> e + {neu}",

                         {'pos': pos, 'neu': neu},

                         ETemperaturePower(1e-19 * co.centi**6, 4.5),

                         generic="e + e + A+ -> e + A",

                         ref="Kossyi1992/PSST")


        self.add_pattern("e + {pos} + M -> {neu} + M",

                         {'pos': pos, 'neu': neu},

                         ETemperaturePower(6e-27 * co.centi**6, 1.5),

                         generic="e + A+ + M -> A + M",

                         ref="Kossyi1992/PSST")

        self.initialize()  # Adding reactions



class TemperaturePower(ch.Rate):

    def __init__(self, k0, power, T0=300):

        self.k0 = k0

        self.power = power

        self.T0 = T0



    def __call__(self, EN, T):

        return full_like(EN, self.k0 * (self.T0 / T)** self.power)



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T)^{\num{%g}}$"

                % (self.k0, self.T0, self.power))


class TemperatureExponential(ch.Rate):

    def __init__(self, k0, c1, T0=300):

        self.k0 = k0

        self.c1 = c1

        self.T0 = T0




    def __call__(self, EN, T):

        return full_like(EN, self.k0 * exp(self.c1 / T))



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T_e)^{\num{%g}}$"

                % (self.k0, self.c1, self.c2, self.T0, self.power))


class ETemperatureExponential(ch.Rate):

    def __init__(self, k0, c1, c2, power, T0=300):

        self.k0 = k0

        self.power = power

        self.c1 = c1

        self.c2 = c2



    def __call__(self, EN, T):

        Te = T + 3648.6 * (EN)**0.46

        return full_like(EN, self.k0 * (300.0 /Te) ** self.power * exp(-self.c1/T) * exp((self.c2*(Te - T))/(Te * T)))



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T_e)^{\num{%g}}$"

                % (self.k0, self.c1, self.c2, self.T0, self.power))


class ETemperaturePower(ch.Rate):

    def __init__(self, k0, power, T0=300):

        self.k0 = k0

        self.power = power

        self.T0 = T0


    def __call__(self, EN, T):

        Te = T + 3648.6 * (EN)**0.46

        return full_like(EN, self.k0 * (self.T0 / Te)** self.power)



    def latex(self):

        return (r"$\num{%g} \times (\num{%g} / T_e)^{\num{%g}}$"

                % (self.k0, self.T0, self.power))


class PancheshnyiFitEN(ch.Rate):

    def __init__(self, k0, a, b):

        self.k0 = k0

        self.a = a

        self.b = b



    def __call__(self, EN, T):

        return self.k0 * exp(-(self.a / (self.b + EN))**2)



    def latex(self):

        self.k_0 = self.k0

        params = ["$%s = \\num{%g}$" % (s, getattr(self, s))

                  for s in ('k_0', 'a', 'b')]



        return ("$k_0e^{-\\left(\\frac{a}{b + E/n}\\right)^2}$ [%s]"

                % ", ".join(params))


class PancheshnyiFitEN2(ch.Rate):

    def __init__(self, k0, a):

        self.k0 = k0

        self.a = a



    def latex(self):

        self.k_0 = self.k0

        params = ["$%s = \\num{%g}$" % (s, getattr(self, s))

                  for s in ('k_0', 'a')]



        return ("$k_0e^{-\\left(\\frac{E/n}{a}\\right)^2}$ [%s]"

                % ", ".join(params))



    def __call__(self, EN, T):

        return self.k0 * exp(-(EN / self.a)**2)


class ranalytic(ch.Rate):

    def __init__(self, a, b, c, d):

        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def __call__(self, EN, T):
        logk = self.a*np.log10(EN)**self.b + self.c/EN + self.d*(1.<EN)
        return co.centi**3*10**logk


#-------------------------------------------------------------------------------

def chemical(rs,q,en,tgas=None):
    tgas = 200
    dn_dt = rs.fderivs(q,en,tgas)

    return dn_dt


if __name__ == '__main__':

    main()
