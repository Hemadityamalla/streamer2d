# Streamer2D

## What is Streamer2D?

It is a 2-D cylindrically symmetric code to simulate a single streamer discharge. 
It is built upon PETCLAW (CLAWPACK), a distributed-memory solver for non-linear 
hyperbolic system of equations.

This code consists of:

* **CLAWPACK 5.0:**
  [Clawpack](http://www.clawpack.Conservation Laws )
  is a collection of finite volume methods for linear and
  nonlinear hyperbolic systems of conservation laws.
  Clawpack employs high-resolution Godunov-type methods with limiters
  in a general framework applicable to many kinds of waves.
  
  ***The version provided here is a modification built upon***
  *** the commit 5714366323acc0d571e97848ba9cbb53e0129de0***

* **/sprite2D/streamer2D.py:**
  Python code to solve streamer equations.

* **/sprite2D/poisson_mg.py:**
  Poisson and Helmholtz equations solver.

* **/sprite2D/riemann.f90:**
  Drift equations Riemann solver.

* **/sprite2D/default.py:**
  Code that uses CHEMISE to evaluate our chemical system.

* **/sprite2D/chemise/swarm:**
  Folder containing air-chemistry reaction rates, mobility and diffusion
  for electrons.

* **/sprite2D/config.cfg:**
  Configuration file for a single sprite streamer discharge.

* **/sprite2D/Initial_conditions:**
  Initial condition files for a single sprite streamer discharge.


## Requeriments
To run this code, first you need to install:

- [CHEMISE](https://gitlab.com/aluque/chemise.git)
- [PETSC](https://www.mcs.anl.gov/petsc/)
- [MPI4Py](http://mpi4py.scipy.org/docs/)
- [petsc4py](https://bitbucket.org/petsc/petsc4py)

Check compatibility when installing CLAWPACK's dependencies